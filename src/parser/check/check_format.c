/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 00:42:49 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/30 15:14:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		check_format(char *line, int line_type)
{
	if (line_type == OPTION_RES)
	{
		if (check_resolution(line))
			return (1);
		return (0);
	}
	else if (line_type == OPTION_COLOR)
	{
		if (check_color(line))
			return (1);
		return (0);
	}
	else if (line_type == OPTION_PATH)
	{
		if (check_valid_path(line))
			return (error_put(ERROR, INVAL_TEXPATH));
		return (0);
	}
	else if (line_type == MAP || line_type == EMPTY)
		return (0);
	return (1);
}
