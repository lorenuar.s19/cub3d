/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_resolution.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 00:25:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/04 19:54:15 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			check_resolution(char *line)
{
	t_vec	res;

	res = (t_vec){0, 0};
	while (line && *line && (is_charset(*line, OPT_RES) || is_wsp(*line)))
		line++;
	res.x = read_num(&line);
	while (line && *line && is_wsp((*line)))
		line++;
	res.y = read_num(&line);
	if (res.x <= 0 || res.y <= 0)
		return (error_put(1, RES_OUT_RANGE));
	if (*line != '\0')
		return (error_put(1, UNEXP_CHAR));
	return (0);
}
