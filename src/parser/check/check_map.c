/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 00:28:08 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:38:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			check_map(char *line)
{
	size_t	i;
	int		ret;

	i = 0;
	while (line && line[i] && is_wsp(line[i]))
		i++;
	while (line && line[i])
	{
		if (!(ret = is_charset(line[i], VALID_MAP_CHARS)))
			return (error_put(INVALID, INVALID_CHAR));
		i++;
	}
	return (MAP);
}
