/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_color.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 00:13:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:38:38 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static	int		check_color_chars(char *line)
{
	while (line && *line)
	{
		if (!is_charset(*line, "0123456789\n\t\v\r ,FC"))
		{
			return (1);
		}
		line++;
	}
	return (0);
}

int				check_color(char *line)
{
	t_scolor	col;

	col = (t_scolor){-1, -1, -1, -1};
	if (check_color_chars(line))
		return (error_put(1, INVALID_CHAR));
	while (line && *line && (is_charset(*line, OPT_COLOR) || is_wsp(*line)))
		line++;
	col.r = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.g = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.b = read_num(&line);
	if (col.r > 255 || col.g > 255 || col.b > 255)
		return (error_put(1, COLOR_RANGE));
	if (*line != '\0')
		return (error_put(1, UNEXP_CHAR));
	return (0);
}
