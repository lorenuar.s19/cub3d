/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_order.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/31 12:36:26 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:39:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int				check_order(t_fstat *fstat, char *line)
{
	if (line && *line && (
		fstat->lin_typ == OPTION_RES ||
		fstat->lin_typ == OPTION_COLOR ||
		fstat->lin_typ == OPTION_PATH))
		fstat->opt_lin_num++;
	return (0);
}
