/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_extension.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 16:57:55 by lorenuar          #+#    #+#             */
/*   Updated: 2020/10/29 11:25:00 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		check_extension(char *path)
{
	char *str;

	if (!path || !*path)
		return (1);
	if (!(str = str_has(path, ".cub", str_len(path))))
		return (1);
	return (0);
}
