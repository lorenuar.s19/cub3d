/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_empty_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/10 22:54:27 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/10 23:15:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			check_empty_line(char *line)
{
	size_t i;

	if (line && !*line)
		return (1);
	i = 0;
	while (line && line[i])
	{
		if (!is_wsp(line[i]))
			return (0);
		i++;
	}
	if (line[i] == '\0')
		return (1);
	return (0);
}
