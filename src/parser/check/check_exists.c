/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_exists.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 19:51:09 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/11 01:21:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			check_exists(t_fstat *fstat)
{
	if (fstat->opt_lin_num != NUM_TYPS)
		return (1);
	return (0);
}
