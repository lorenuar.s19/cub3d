/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_valid_path.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 01:48:10 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/11 10:02:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		check_valid_path(char *line)
{
	int	fd;

	while (line && *line && (is_charset(*line, OPT_PATH) || is_wsp(*line)))
	{
		line++;
	}
	if ((fd = open(line, O_RDONLY)) == -1)
	{
		return (error_sys_put(errno));
	}
	if (close(fd) == -1)
	{
		return (error_sys_put(errno));
	}
	return (0);
}
