/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fstats.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 01:12:09 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/11 01:24:14 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			read_fstats(char *line, t_fstat *fstat, t_fdata *fdata)
{
	size_t	len;

	(void)fdata;
	len = str_len(line);
	if ((fstat->lin_typ = lintyp_get_category(line)) == -1)
		return (1);
	if (check_format(line, fstat->lin_typ))
		return (1);
	if (len > fstat->max_lin_len)
		fstat->max_lin_len = len;
	check_order(fstat, line);
	if (fstat->lin_typ == MAP)
	{
		fstat->curr_section = MAP;
		fstat->map_lin_num++;
		if (len > fstat->map_max_len)
			fstat->map_max_len = len;
	}
	if (fstat->curr_section == MAP && fstat->lin_typ == EMPTY)
	{
		return (error_put(1, EMPTY_MAP));
	}
	fstat->tot_lin++;
	return (0);
}
