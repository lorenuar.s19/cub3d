/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fdata.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 22:51:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:40:30 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			read_fdata(char *line, t_fstat *fstat, t_fdata *fdata)
{
	if ((fstat->lin_typ = lintyp_get_category(line)) == -1)
		return (1);
	if (fstat->lin_typ == EMPTY)
		return (0);
	if ((fstat->opt_typ = lintyp_get_opt(line, 0)) == INVALID &&
		fstat->lin_typ != MAP)
		return (1);
	if (fstat->lin_typ != MAP && get_fdata(line, fstat, fdata))
		return (1);
	if (fstat->lin_typ == MAP && map_parse_strs(line, fdata))
		return (error_put(1, BAD_MAP));
	return (0);
}
