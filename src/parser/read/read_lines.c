/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_lines.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 16:11:56 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/27 18:37:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			read_lines(char *path, t_fstat *fstat, t_fdata *fdata,
						int (sub_exec_ptr)(char *, t_fstat *, t_fdata *))
{
	char	*line;
	int		ret;

	if (!path || !fstat || !fdata || !sub_exec_ptr)
		return (1);
	if ((fstat->fd = open(path, O_RDONLY)) < 0)
		return (error_sys_put(errno));
	while ((ret = get_next_line(fstat->fd, &line)) > 0)
	{
		if ((sub_exec_ptr)(line, fstat, fdata))
			return (free_str_error(1, FILE_FORMAT, &line));
		free_str(&(line));
	}
	if (ret != 0 && ret != 1)
	{
		return (error_put(1, ERR_GNL));
	}
	fstat->curr_section = 0;
	if ((sub_exec_ptr)(line, fstat, fdata))
		return (free_str_error(1, FILE_FORMAT, &line));
	free_str(&(line));
	if (close(fstat->fd) == -1)
		return (error_sys_put(errno));
	return (0);
}
