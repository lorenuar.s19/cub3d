/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 17:55:22 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 14:13:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	parse_map(t_fstat *fstat, t_fdata *fdata)
{
	if (map_get_player(fdata))
	{
		return (1);
	}
	if (map_get_sprites(fdata))
	{
		return (1);
	}
	if (!(fdata->visited.tab = init_map(fstat, fdata)))
	{
		free_map(&(fdata->visited));
		return (error_sys_put(errno));
	}
	fdata->visited.siz = fdata->map.siz;
	if ((map_check_closed(fdata->map.siz, fdata, fdata->player.pos)))
	{
		free_map(&(fdata->visited));
		return (error_put(1, MCHK_OPEN));
	}
	free_map(&(fdata->visited));
	return (0);
}

int			parse_file(char *path, t_fstat *fstat, t_fdata *fdata)
{
	if (check_extension(path))
		return (error_put(1, BAD_EXT));
	if (read_lines(path, fstat, fdata, read_fstats))
		return (1);
	if (check_exists(fstat))
		return (error_put(1, OPT_INVAL));
	if (!(fdata->map.tab = init_map(fstat, fdata)))
		return (error_sys_put(errno));
	if (read_lines(path, fstat, fdata, read_fdata))
		return (1);
	if (parse_map(fstat, fdata))
	{
		return (error_put(1, MAP_ERR));
	}
	return (0);
}
