/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lintyp_get_category.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 19:31:35 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/10 22:59:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			lintyp_get_category(char *line)
{
	if (line && check_empty_line(line))
		return (EMPTY);
	if (line && str_start_with(line, OPT_RES))
		return (OPTION_RES);
	else if (line && (str_start_with(line, OPT_NORTH) ||
		str_start_with(line, OPT_SOUTH) ||
		str_start_with(line, OPT_WEST) ||
		str_start_with(line, OPT_EAST) ||
		str_start_with(line, OPT_SPRIT)))
		return (OPTION_PATH);
	else if (line && (str_start_with(line, OPT_FLOOR) ||
		str_start_with(line, OPT_CEIL)))
		return (OPTION_COLOR);
	return (check_map(line));
}
