/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lintyp_get_opt.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 18:59:16 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/09 12:20:19 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "parser/parser_opt_lintyps.h"

int				lintyp_get_opt(char *line, size_t lin_num)
{
	ssize_t		i;

	i = lin_num;
	while (i >= 0 && i < NUM_TYPS)
	{
		if (str_start_with(line, g_checkord_str[i]))
			return (g_lintyps[i]);
		i++;
	}
	return (INVALID);
}
