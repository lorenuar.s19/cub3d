/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lintyp_get_map.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 21:21:59 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/15 13:52:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"
#include "parser/parser_map_lintyps.h"

int			lintyp_get_map(char map)
{
	int		i;

	i = 0;
	while (i < MAP_TYPS)
	{
		if (is_charset(map, g_map_charsets[i]))
			return (g_map_typs[i]);
		i++;
	}
	return (INVALID);
}
