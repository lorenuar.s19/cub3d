/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 00:12:21 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/10 10:31:40 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

char		*init_map(t_fstat *fstat, t_fdata *fdata)
{
	size_t	siz;
	char	*map;

	fdata->map.siz.x = fstat->map_max_len;
	fdata->map.siz.y = fstat->map_lin_num;
	siz = (sizeof(char) * (fstat->map_max_len * fstat->map_lin_num)) + 1;
	if (!(map = (char *)alloc_zeroed(siz)))
		return (NULL);
	return (map);
}
