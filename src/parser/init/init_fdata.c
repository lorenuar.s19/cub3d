/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fdata.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 21:53:10 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/15 14:05:30 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_fdata		init_fdata(void)
{
	t_fdata fdata;

	fdata.res = (t_vec){0, 0};
	fdata.tex_paths[T_NORTH] = NULL;
	fdata.tex_paths[T_SOUTH] = NULL;
	fdata.tex_paths[T_WEST] = NULL;
	fdata.tex_paths[T_EAST] = NULL;
	fdata.tex_paths[T_SPRIT] = NULL;
	fdata.floor = 0;
	fdata.ceil = 0;
	fdata.map = (t_map){(t_vec){0, 0}, NULL};
	fdata.visited = (t_map){(t_vec){0, 0}, NULL};
	fdata.player = (t_ch_player){-1, (t_vec){-1, -1}};
	return (fdata);
}
