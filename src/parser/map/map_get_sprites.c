/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_get_sprites.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/30 13:45:31 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/01 23:04:26 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	map_count_sprites(t_fdata *fdata)
{
	t_vec	pos;
	t_vec	siz;

	pos = (t_vec){0, 0};
	siz = fdata->map.siz;
	fdata->num_sprit = 0;
	while (pos.y < siz.y)
	{
		pos.x = 0;
		while (pos.x < siz.x)
		{
			if (fdata->map.tab[(pos.y * siz.x) + pos.x] ==
													M_SPRIT)
			{
				fdata->num_sprit++;
			}
			pos.x++;
		}
		pos.y++;
	}
}

static void	map_set_sprit_data(t_fdata *fdata)
{
	int		i;
	t_vec	pos;
	t_vec	siz;

	pos = (t_vec){0, 0};
	siz = fdata->map.siz;
	i = 0;
	while (pos.y < siz.y)
	{
		pos.x = 0;
		while (pos.x < siz.x && i < fdata->num_sprit)
		{
			if (fdata->map.tab[
				(pos.y * siz.x) + pos.x] == M_SPRIT &&
				i < fdata->num_sprit)
			{
				fdata->sprites[i] = (t_fvec){pos.x + 0.5, pos.y + 0.5};
				i++;
			}
			pos.x++;
		}
		pos.y++;
	}
}

static int	map_init_sprites(t_fdata *fdata)
{
	if (!(fdata->sprites = malloc(
			fdata->num_sprit * sizeof(t_fvec))))
	{
		return (error_sys_put(errno));
	}
	return (0);
}

int			map_get_sprites(t_fdata *fdata)
{
	map_count_sprites(fdata);
	if (map_init_sprites(fdata))
	{
		return (error_put(1, MAP_INIT_SPRIT));
	}
	map_set_sprit_data(fdata);
	return (0);
}
