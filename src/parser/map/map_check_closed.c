/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_check_closed.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/11 10:45:31 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:44:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int				pos_valid(t_vec siz, t_vec pos)
{
	if (pos.x >= 0 && pos.x < siz.x && pos.y >= 0 && pos.y < siz.y)
	{
		return (1);
	}
	return (0);
}

static int				bound_check(t_vec siz, t_vec pos, char map)
{
	if (pos.x >= siz.x - 1 || pos.x <= 0 || pos.y >= siz.y - 1 || pos.y <= 0)
	{
		if (map != M_WALL)
		{
			return (1);
		}
	}
	return (0);
}

static int				push_next_pos(t_stack **stack, t_fdata *fdata,
												t_vec ofspos, t_vec siz)
{
	if (bound_check(siz, ofspos, fdata->map.tab[(ofspos.y * siz.x) + ofspos.x]))
	{
		return (error_put(1, MCHK_BOUND));
	}
	if (!fdata->visited.tab[(ofspos.y * siz.x) + ofspos.x] &&
		((fdata->map.tab[(ofspos.y * siz.x) + ofspos.x] >= M_SPRIT &&
		fdata->map.tab[(ofspos.y * siz.x) + ofspos.x] <= M_EAST) ||
		(fdata->map.tab[(ofspos.y * siz.x) + ofspos.x] >= 0 &&
		fdata->map.tab[(ofspos.y * siz.x) + ofspos.x] <= M_EMPTY_INSIDE)))
	{
		if (!vect_push(stack, vect_new((t_vec){ofspos.x, ofspos.y})))
		{
			return (error_put(1, VECT_PUSH));
		}
	}
	return (0);
}

static int				map_check_closed_sub(t_stack **stack, t_fdata *fdata,
														t_vec pos, t_vec siz)
{
	t_vec				ofspos;
	static const int	ofs[2][4] = {{0, 1, 0, -1}, {-1, 0, 1, 0}};
	int					i;

	i = 0;
	while (i < 4 && pos_valid(siz, pos))
	{
		ofspos = (t_vec){pos.x + ofs[0][i], pos.y + ofs[1][i]};
		if (fdata->visited.tab[(ofspos.y * siz.x) + ofspos.x])
		{
			i++;
			continue ;
		}
		if (push_next_pos(stack, fdata, ofspos, siz))
			return (1);
		fdata->visited.tab[(ofspos.y * siz.x) + ofspos.x] = 1;
		i++;
	}
	return (0);
}

int						map_check_closed(t_vec siz, t_fdata *fdata, t_vec pos)
{
	t_stack				*stack;

	if (!pos_valid(siz, pos) || !fdata)
		return (error_put(1, PLAYER_OUTMAP));
	stack = NULL;
	if (!vect_push(&stack, vect_new(pos)))
		return (error_put(1, VECT_PUSH));
	while (stack)
	{
		pos = *((t_vec *)stack->data);
		if (map_check_closed_sub(&stack, fdata, pos, siz))
		{
			while (stack)
				vect_pop(&stack);
			return (1);
		}
		vect_pop(&stack);
	}
	while (stack)
		vect_pop(&stack);
	return (0);
}
