/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_get_player.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/11 10:50:40 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:16:17 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int				player_get_direction(char get_dir)
{
	static const int	dir_dirs[NUM_OR] = {NORTH, SOUTH, EAST, WEST};
	static const int	dir_maps[NUM_OR] = {M_NORTH, M_SOUTH, M_EAST, M_WEST};
	int					i;

	i = 0;
	while (i < NUM_OR && get_dir != 0)
	{
		if (get_dir == dir_maps[i])
			return (dir_dirs[i]);
		i++;
	}
	return (INVALID);
}

static int				search_line(t_fdata *fdata, int *dir,
										t_vec *pos, t_vec siz)
{
	char	chr;

	while (pos->x < siz.x)
	{
		chr = fdata->map.tab[(pos->y * siz.x) + pos->x];
		if ((*dir = player_get_direction(chr)) != INVALID)
		{
			fdata->player = (t_ch_player){*dir, *pos};
		}
		pos->x++;
	}
	return (0);
}

int						map_get_player(t_fdata *fdata)
{
	int		dir;
	t_vec	pos;
	t_vec	siz;

	pos = (t_vec){0, 0};
	siz = fdata->map.siz;
	while (pos.y < siz.y)
	{
		pos.x = 0;
		if (search_line(fdata, &dir, &pos, siz))
			return (1);
		pos.y++;
	}
	if (fdata->player.dir == -1)
	{
		return (error_put(1, PLAYER_NTFND));
	}
	return (0);
}
