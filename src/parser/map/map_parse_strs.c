/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_parse_strs.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 00:08:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:40:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int					map_parse_strs(char *line, t_fdata *fdata)
{
	static size_t	curr_lin = 0;
	size_t			i;
	int				lintyp;

	if ((ssize_t)curr_lin > fdata->map.siz.y)
		return (1);
	i = 0;
	while ((ssize_t)i < fdata->map.siz.x && line && line[i])
	{
		if ((lintyp = lintyp_get_map(line[i])) == -1)
		{
			return ((error_put(1, INVAL_LINTYP)));
		}
		fdata->map.tab[(curr_lin * fdata->map.siz.x) + i] = lintyp;
		i++;
	}
	curr_lin++;
	return (0);
}
