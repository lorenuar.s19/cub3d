/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_fdata.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 01:02:21 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 18:51:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		free_fdata(t_fdata *fdata)
{
	size_t	i;

	free_map(&(fdata->map));
	free_map(&(fdata->visited));
	fdata->map = (t_map){(t_vec){0, 0}, NULL};
	fdata->visited = (t_map){(t_vec){0, 0}, NULL};
	i = 0;
	free(fdata->sprites);
	fdata->sprites = NULL;
	while (i < NUM_TEXTURES)
	{
		free_str(&(fdata->tex_paths[i]));
		i++;
	}
}
