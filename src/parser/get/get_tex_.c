/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tex_.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/04 11:17:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/30 15:11:10 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		get_tex_north(char *line, t_fdata *fdata)
{
	if (line && !str_start_with(line, OPT_NORTH))
	{
		return (error_put(1, OPT_KEY));
	}
	while (line && *line && (is_charset(*line, OPT_NORTH) || is_wsp(*line)))
	{
		line++;
	}
	fdata->tex_paths[T_NORTH] = str_dupli(line);
	return (0);
}

int		get_tex_south(char *line, t_fdata *fdata)
{
	if (line && !str_start_with(line, OPT_SOUTH))
	{
		return (error_put(1, OPT_KEY));
	}
	while (line && *line && (is_charset(*line, OPT_SOUTH) || is_wsp(*line)))
	{
		line++;
	}
	fdata->tex_paths[T_SOUTH] = str_dupli(line);
	return (0);
}

int		get_tex_west(char *line, t_fdata *fdata)
{
	if (line && !str_start_with(line, OPT_WEST))
	{
		return (error_put(1, OPT_KEY));
	}
	while (line && *line && (is_charset(*line, OPT_WEST) || is_wsp(*line)))
	{
		line++;
	}
	fdata->tex_paths[T_WEST] = str_dupli(line);
	return (0);
}

int		get_tex_east(char *line, t_fdata *fdata)
{
	if (line && !str_start_with(line, OPT_EAST))
	{
		return (error_put(1, OPT_KEY));
	}
	while (line && *line && (is_charset(*line, OPT_EAST) || is_wsp(*line)))
	{
		line++;
	}
	fdata->tex_paths[T_EAST] = str_dupli(line);
	return (0);
}

int		get_tex_sprite(char *line, t_fdata *fdata)
{
	if (line && !str_start_with(line, OPT_SPRIT))
	{
		return (error_put(1, OPT_KEY));
	}
	while (line && *line && (is_charset(*line, OPT_SPRIT) || is_wsp(*line)))
	{
		line++;
	}
	fdata->tex_paths[T_SPRIT] = str_dupli(line);
	return (0);
}
