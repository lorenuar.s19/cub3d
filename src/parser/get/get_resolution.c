/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_resolution.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/04 11:14:20 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/05 17:05:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		get_resolution(char *line, t_fdata *fdata)
{
	while (line && *line && (is_charset(*line, OPT_RES) || is_wsp(*line)))
		line++;
	fdata->res.x = read_num(&line);
	while (line && *line && is_wsp((*line)))
		line++;
	fdata->res.y = read_num(&line);
	if (fdata->res.x < 0 || fdata->res.y < 0)
		return (1);
	return (0);
}
