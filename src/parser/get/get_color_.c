/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color_.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/04 17:19:46 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:39:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			get_col_floor(char *line, t_fdata *fdata)
{
	t_scolor	col;

	col = (t_scolor){-1, -1, -1, -1};
	while (line && *line && (is_charset(*line, OPT_COLOR) || is_wsp(*line)))
		line++;
	col.r = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.g = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.b = read_num(&line);
	if (col.r > 255 || col.g > 255 || col.b > 255)
		return (1);
	fdata->floor = build_rgb(col.r, col.g, col.b);
	return (0);
}

int			get_col_ceil(char *line, t_fdata *fdata)
{
	t_scolor	col;

	col = (t_scolor){-1, -1, -1, -1};
	while (line && *line && (is_charset(*line, OPT_COLOR) || is_wsp(*line)))
		line++;
	col.r = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.g = read_num(&line);
	while (line && *line && (is_wsp(*line) || *line == ','))
		line++;
	col.b = read_num(&line);
	if (col.r > 255 || col.g > 255 || col.b > 255)
		return (1);
	fdata->ceil = build_rgb(col.r, col.g, col.b);
	return (0);
}
