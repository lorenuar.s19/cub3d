/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_fdata.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 23:21:26 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/05 18:36:59 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			get_fdata(char *line, t_fstat *fstat, t_fdata *fdata)
{
	static int (*get_methods[NUM_TYPS])(char *, t_fdata *) = {
		get_resolution,
		get_tex_north,
		get_tex_south,
		get_tex_west,
		get_tex_east,
		get_tex_sprite,
		get_col_floor,
		get_col_ceil
	};

	if (fstat->opt_typ < NUM_TYPS && fstat->opt_typ >= 0)
	{
		if (get_methods[fstat->opt_typ](line, fdata))
			return (1);
	}
	return (0);
}
