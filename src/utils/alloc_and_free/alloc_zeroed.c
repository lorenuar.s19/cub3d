/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alloc_zeroed.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 00:15:01 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 22:30:17 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		*alloc_zeroed(size_t size)
{
	size_t	i;
	char	*new;

	if (!(new = malloc(size + 1)))
		return (NULL);
	i = 0;
	while (new && i < size)
	{
		new[i] = 0;
		i++;
	}
	new[i] = 0;
	return (new);
}
