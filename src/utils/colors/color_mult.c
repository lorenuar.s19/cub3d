/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_mult.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 02:49:29 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:48:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	set_mult(t_col *col, double mult)
{
	if (mult * (*col) >= 255)
	{
		(*col) = 255;
	}
	else if (mult * (*col) <= 0)
	{
		(*col) = 0;
	}
	else
	{
		(*col) = (*col) * mult;
	}
}

t_color		color_mult(t_color color, double mult)
{
	t_scolor col;

	col = (t_scolor){
		get_alpha(color),
		get_red(color),
		get_green(color),
		get_blue(color)
		};
	set_mult(&(col.r), mult);
	set_mult(&(col.g), mult);
	set_mult(&(col.b), mult);
	return (build_argb(col.a, col.r, col.g, col.b));
}

static void	set_div(t_col *col, double div)
{
	if (div == 0)
	{
		return ;
	}
	if ((*col) / div >= 255)
	{
		(*col) = 255;
	}
	else if ((*col) / div <= 0)
	{
		(*col) = 0;
	}
	else
	{
		(*col) = (*col) / div;
	}
}

t_color		color_div(t_color color, double div)
{
	t_scolor col;

	col = (t_scolor){
		get_alpha(color),
		get_red(color),
		get_green(color),
		get_blue(color)
		};
	set_div(&(col.r), div);
	set_div(&(col.g), div);
	set_div(&(col.b), div);
	return (build_argb(col.a, col.r, col.g, col.b));
}
