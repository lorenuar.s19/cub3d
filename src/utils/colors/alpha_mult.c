/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alpha_mult.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 16:13:04 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:49:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static double	alpha_mult_color(double src, double dst, double alpha)
{
	return (src * (alpha) + dst * (1.0 - alpha));
}

t_color			alpha_mult(t_color colsrc, t_color coldst)
{
	t_scolor	src;
	t_scolor	dst;
	t_scolor	new;
	double		alpha;

	src = get_scolor(colsrc);
	dst = get_scolor(coldst);
	alpha = src.a / 255.0;
	new.r = alpha_mult_color(src.r / 255.0, dst.r / 255.0, alpha) * 255.0;
	new.g = alpha_mult_color(src.g / 255.0, dst.g / 255.0, alpha) * 255.0;
	new.b = alpha_mult_color(src.b / 255.0, dst.b / 255.0, alpha) * 255.0;
	return (build_scolor(new));
}
