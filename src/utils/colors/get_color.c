/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:21:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:46:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_col		get_alpha(t_color color)
{
	return ((color & (0xff << 24)) >> 24);
}

t_col		get_red(t_color color)
{
	return ((color & (0xff << 16)) >> 16);
}

t_col		get_green(t_color color)
{
	return ((color & (0xff << 8)) >> 8);
}

t_col		get_blue(t_color color)
{
	return ((t_col)(color & 0xff));
}

t_scolor	get_scolor(t_color src)
{
	return ((t_scolor){
		get_alpha(src),
		get_red(src),
		get_green(src),
		get_blue(src)});
}
