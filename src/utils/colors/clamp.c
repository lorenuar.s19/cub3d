/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clamp.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 16:14:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/25 18:00:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_col	clamp(t_col val)
{
	if (val >= UCHAR_MAX)
	{
		return (UCHAR_MAX);
	}
	if (val <= 0)
	{
		return (0);
	}
	return (val);
}
