/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_color.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 03:05:44 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:45:29 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_color			build_argb(t_col a, t_col r, t_col g, t_col b)
{
	return (
		clamp(a) << 24 |
		clamp(r) << 16 |
		clamp(g) << 8 |
		clamp(b));
}

t_color			build_rgb(t_col r, t_col g, t_col b)
{
	return (
		255 << 24 |
		clamp(r) << 16 |
		clamp(g) << 8 |
		clamp(b));
}

t_color			build_scolor(t_scolor col)
{
	return (
		clamp(col.a) << 24 |
		clamp(col.r) << 16 |
		clamp(col.g) << 8 |
		clamp(col.b));
}
