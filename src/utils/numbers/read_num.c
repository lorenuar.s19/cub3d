/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_num.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 17:33:42 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 22:28:12 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

ssize_t		read_num(char **s)
{
	ssize_t	num;
	int		sign;

	num = 0;
	sign = 1;
	while (s && *s && **s && is_wsp(**s))
		(*s)++;
	if (s && *s && **s == '-')
		sign = -1;
	if (s && *s && (**s == '-' || **s == '+'))
		(*s)++;
	while (s && *s && **s >= '0' && **s <= '9')
	{
		num = (num * 10) + (**s - '0');
		(*s)++;
	}
	if (num > LONG_MAX)
		return ((sign == 1) ? -1 : 0);
	return ((sign == 1) ? num : -num);
}
