/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 17:47:38 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 22:27:17 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_stack		*stack_new(void *data)
{
	t_stack	*new;

	if (!(new = (t_stack *)malloc(sizeof(t_stack))))
	{
		return (NULL);
	}
	new->data = data;
	new->next = NULL;
	return (new);
}
