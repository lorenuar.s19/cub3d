/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_pop.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/20 15:47:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 22:27:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			vect_pop(t_stack **stack)
{
	t_stack	*tmp;

	if (!stack || !*stack)
	{
		return (0);
	}
	tmp = (*stack)->next;
	if ((*stack)->data)
	{
		free((*stack)->data);
		(*stack)->data = NULL;
	}
	free(*stack);
	*stack = tmp;
	return (1);
}
