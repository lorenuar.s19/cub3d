/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vect_new.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 17:47:38 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 22:27:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_stack		*vect_new(t_vec vec)
{
	t_vec	*new_vec;
	t_stack	*new;

	if (!(new = (t_stack *)malloc(sizeof(t_stack))))
	{
		return (NULL);
	}
	if (!(new_vec = (t_vec *)malloc(sizeof(t_vec))))
	{
		return (NULL);
	}
	*new_vec = vec;
	new->data = new_vec;
	new->next = NULL;
	return (new);
}
