/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_push.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 18:01:28 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/23 10:35:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_stack		*stack_push(t_stack **stack, t_stack *new)
{
	if (!stack || !new)
	{
		return (NULL);
	}
	new->next = *stack;
	*stack = new;
	return (*stack);
}
