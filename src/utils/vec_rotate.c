/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_rotate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 01:35:10 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/25 15:30:29 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

t_fvec	vec_rot(t_fvec in, double rot)
{
	return ((t_fvec){
		in.x * cos(rot) - in.y * sin(rot),
		in.x * sin(rot) + in.y * cos(rot)
		});
}
