/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_times_endl.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 17:26:12 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:35:56 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

ssize_t		str_times_endl(char *s, ssize_t n, size_t len)
{
	ssize_t ret;

	ret = 0;
	while (n && s)
	{
		if (len == 0)
		{
			ret += write(1, s, str_len(s));
		}
		else
		{
			ret += write(1, s, len);
		}
		(n > 0) ? (n--) : (n++);
	}
	if (write(1, "\n", 1) == -1)
	{
		return (error_sys_put(errno));
	}
	return (ret);
}
