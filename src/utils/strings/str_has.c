/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_has.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 16:55:42 by lorenuar          #+#    #+#             */
/*   Updated: 2020/10/24 22:07:43 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

char	*str_has(const char *s, const char *f, size_t size)
{
	size_t i;
	size_t j;

	i = 0;
	j = 0;
	if (!f[0])
		return ((char*)s);
	while (s && f && s[i] && i < size)
	{
		j = 0;
		while (f[j] == s[i + j] && f[j] && s[i + j])
			j++;
		if (f[j] == 0 && i + j <= size)
			return ((char*)s + i);
		i++;
	}
	return (NULL);
}
