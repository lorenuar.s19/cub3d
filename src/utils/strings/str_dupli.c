/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_dupli.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 17:34:51 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/28 17:07:57 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

char	*str_dupli(const char *to_copy)
{
	char	*new;
	int		i;

	i = 0;
	if (!(new = malloc((str_len(to_copy) + 1) * sizeof(char))))
		return (NULL);
	while (to_copy && *to_copy)
		new[i++] = *to_copy++;
	new[i] = '\0';
	return (new);
}
