/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_start_with.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 16:55:42 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/05 16:59:11 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			str_start_with(const char *s, const char *f)
{
	size_t i;
	size_t size;

	i = 0;
	size = str_len(f) - 1;
	while (s && f && s[i] && f[i] && s[i] == f[i] && i < size)
		i++;
	if (s && f && s[i] == f[i])
	{
		return (1);
	}
	return (0);
}
