/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_frame.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 17:10:25 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 12:38:03 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int		draw_frame_utils(t_mlx *mlx, long *frameskip,
										long *framecount, int *already_called)
{
	(*frameskip)++;
	(*framecount)++;
	if ((*frameskip) < SKIPPED_FRAMES)
	{
		return (1);
	}
	if ((*frameskip) > SKIPPED_FRAMES)
	{
		(*frameskip) = 0;
	}
	if (mlx->curr_img == mlx->img1)
	{
		mlx->curr_img = mlx->img2;
	}
	else
	{
		mlx->curr_img = mlx->img1;
	}
	if (!(*already_called))
	{
		(*already_called) = 1;
	}
	return (0);
}

int				draw_frame(t_mlx *mlx)
{
	static long	frameskip = 0;
	static long	framecount = 0;
	static int	already_called = 0;

	if (!mlx)
	{
		return (error_put(1, NULLMLX_DRAW));
	}
	if (draw_frame_utils(mlx, &frameskip, &framecount, &already_called))
	{
		if (mlx->show_window)
			return (0);
	}
	put_clear(mlx->curr_img);
	rays_cast(mlx);
	draw_sprites(mlx);
	minimap_draw(mlx);
	player_move(mlx);
	if (mlx->show_window)
	{
		mlx_put_image_to_window(mlx->ptr, mlx->win, mlx->curr_img->img, 0, 0);
	}
	return (0);
}
