/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap_draw_cell.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/18 18:41:18 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:40:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	cell_draw_rect(t_mlx *mlx, t_ivec po, int cell_siz, t_color color)
{
	double	scale;

	scale = mlx->ddat->minimap.scale;
	put_rect_alpha(mlx->curr_img, (t_ivec){
				cell_siz * scale,
				cell_siz * scale
				}, (t_ivec){
				po.x * cell_siz * scale,
				po.y * cell_siz * scale}, color);
	if (scale > 1)
	{
		put_rect_alpha(mlx->curr_img, (t_ivec){1, cell_siz * scale},
			(t_ivec){
				po.x * cell_siz * scale,
				po.y * cell_siz * scale
				}, build_argb(get_alpha(color), 0, 0, 0));
		put_rect_alpha(mlx->curr_img, (t_ivec){cell_siz * scale, 1},
			(t_ivec){
				po.x * cell_siz * scale,
				po.y * cell_siz * scale
			}, build_argb(get_alpha(color), 0, 0, 0));
	}
}

void		draw_minimap_cell(t_mlx *mlx, t_ivec po, int cell_siz, t_color col)
{
	char	curr;

	curr = mlx->fdata->map.tab[(po.y * mlx->fdata->map.siz.x) + po.x];
	if (curr == M_WALL)
	{
		col = mlx->ddat->minimap.wall_col;
	}
	else if (curr == M_SPRIT)
	{
		col = mlx->ddat->minimap.sprit_col;
	}
	else if (curr >= M_NORTH && curr <= M_EAST)
	{
		col = mlx->ddat->minimap.mapplayer_col;
	}
	else
	{
		col = mlx->ddat->minimap.other_col;
	}
	cell_draw_rect(mlx, po, cell_siz, col);
}
