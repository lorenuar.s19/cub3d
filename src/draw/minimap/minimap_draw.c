/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap_draw.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:53:02 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		minimap_draw(t_mlx *mlx)
{
	t_ivec	po;
	t_color	color;

	po = (t_ivec){0, 0};
	color = 0;
	while (po.y < mlx->fdata->map.siz.y)
	{
		po.x = 0;
		while (po.x < mlx->fdata->map.siz.x)
		{
			draw_minimap_cell(mlx, po, mlx->ddat->cell_siz, color);
			po.x++;
		}
		po.y++;
	}
	minimap_draw_player(mlx);
}
