/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap_draw_player.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/18 18:41:29 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:41:10 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	minimap_draw_player_plane(t_mlx *mlx, double scale, t_fvec pos)
{
	put_line_alpha(mlx->curr_img, (t_ivec){
		(pos.x * scale),
		(pos.y * scale)}, (t_ivec){
		(pos.x * scale) + (mlx->ddat->plane.x * scale),
		(pos.y * scale) + (mlx->ddat->plane.y * scale),
		}, mlx->ddat->minimap.dir_col);
	put_line_alpha(mlx->curr_img, (t_ivec){
		(pos.x * scale),
		(pos.y * scale)}, (t_ivec){
		(pos.x * scale) - (mlx->ddat->plane.x * scale),
		(pos.y * scale) - (mlx->ddat->plane.y * scale),
		}, mlx->ddat->minimap.dir_col);
}

void		minimap_draw_player(t_mlx *mlx)
{
	double	scale;
	int		middle_ray;
	t_fvec	pos;
	t_fvec	dir;

	scale = mlx->ddat->minimap.scale * mlx->ddat->cell_siz;
	pos = mlx->ddat->player.pos;
	dir = mlx->ddat->player.dir;
	middle_ray = mlx->ddat->num_rays / 2;
	put_circle_filled_alpha(mlx->curr_img,
	(t_ivec){(pos.x * scale),
			(pos.y * scale)},
			(mlx->ddat->minimap.player_rad * scale) / 4,
			mlx->ddat->minimap.playcirc_col);
	put_line_alpha(mlx->curr_img, (t_ivec){
		(pos.x * scale),
		(pos.y * scale)}, (t_ivec){
		(pos.x * scale) + (dir.x * mlx->ddat->rays[middle_ray].dist * scale),
		(pos.y * scale) + (dir.y * mlx->ddat->rays[middle_ray].dist * scale)
		}, mlx->ddat->minimap.dir_col);
	minimap_draw_player_plane(mlx, scale, pos);
}
