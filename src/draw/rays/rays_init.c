/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/20 18:11:47 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:01:12 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		rays_init(t_mlx *mlx)
{
	int	i;

	i = 0;
	mlx->ddat->num_rays = mlx->curr_img->siz.x;
	if (!((mlx->ddat->rays) = malloc(
						sizeof(*(mlx->ddat->rays)) * (mlx->ddat->num_rays))))
	{
		return (error_sys_put(errno));
	}
	while (i < mlx->ddat->num_rays)
	{
		mlx->ddat->rays[i] = (t_ray){(t_fvec){0, 0}, (t_fvec){0, 0}, 0, 0};
		i++;
	}
	return (0);
}
