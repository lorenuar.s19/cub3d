/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_pixel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/20 17:55:21 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/28 16:29:07 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			get_pixel(t_img *img, t_ivec pos, t_color *color)
{
	char	*dest;

	if (pos.x < 0 || pos.x > img->siz.x || pos.y < 0 || pos.y > img->siz.y)
	{
		return (1);
	}
	dest = img->addr +
				(pos.y * img->line_length + pos.x * (img->bits_per_pixel / 8));
	*color = *(unsigned int *)dest;
	return (0);
}
