/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 17:01:42 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:01:43 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	rays_free(t_mlx *mlx)
{
	if (mlx && mlx->ddat && mlx->ddat->rays)
	{
		free(mlx->ddat->rays);
		mlx->ddat->rays = NULL;
	}
}
