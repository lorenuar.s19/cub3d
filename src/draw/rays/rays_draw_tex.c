/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_draw_tex.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 17:32:21 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/01 22:53:05 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int	get_orientation(t_fvec dir, int side)
{
	if (side == 0)
	{
		if (dir.x > 0)
		{
			return (WEST);
		}
		else
		{
			return (EAST);
		}
	}
	else
	{
		if (dir.y < 0)
		{
			return (NORTH);
		}
		else
		{
			return (SOUTH);
		}
	}
	return (INVALID_OR);
}

static void	get_texel_coord(t_mlx *mlx, t_ray *ray, t_tex *tex)
{
	if (ray->side == 0)
	{
		tex->x_wall = mlx->ddat->player.pos.y + ray->dist * ray->dir.y;
	}
	else
	{
		tex->x_wall = mlx->ddat->player.pos.x + ray->dist * ray->dir.x;
	}
	tex->x_wall -= floor(tex->x_wall);
	if (mlx->ddat->rev_tex)
	{
		tex->x_wall = 1.0 - tex->x_wall;
	}
	tex->tex.x = (int)(tex->x_wall * (double)mlx->ddat->tex[tex->or].siz.x);
	if (ray->side == 0 && ray->dir.x > 0)
	{
		tex->tex.x = mlx->ddat->tex[tex->or].siz.x - tex->tex.x - 1;
	}
	if (ray->side == 1 && ray->dir.y < 0)
	{
		tex->tex.x = mlx->ddat->tex[tex->or].siz.x - tex->tex.x - 1;
	}
}

#define TEX_RAY_MIN_DIST 0.1

static void	rays_setup_tex(t_mlx *mlx, t_ray *ray, t_tex *tex)
{
	double div;

	if (ray->dist > TEX_RAY_MIN_DIST)
		div = ray->dist;
	else
		div = TEX_RAY_MIN_DIST;
	tex->h_wall = (mlx->screensize.y / div);
	tex->y_start = ((mlx->screensize.y / 2) - (tex->h_wall / 2));
	if (tex->y_start <= 0)
	{
		tex->y_start = 0;
	}
	tex->y_end = ((mlx->screensize.y / 2) + (tex->h_wall / 2));
	if (tex->y_end >= mlx->screensize.y)
	{
		tex->y_end = mlx->screensize.y;
	}
	if ((tex->or = get_orientation(ray->dir, ray->side)) > WEST)
	{
		return ;
	}
	get_texel_coord(mlx, ray, tex);
}

void		rays_draw_tex(t_mlx *mlx, t_ray *ray, t_tex *tex, int screenx)
{
	t_color color;
	double	texpos;

	tex->screen.x = screenx;
	rays_setup_tex(mlx, ray, tex);
	tex->step = 1.0 * mlx->ddat->tex[tex->or].siz.y / tex->h_wall;
	texpos = (tex->y_start - mlx->screensize.y / 2 + tex->h_wall / 2) *
																	tex->step;
	tex->screen.y = tex->y_start;
	while (tex->screen.y < (mlx->screensize.y / 2) + (tex->h_wall / 2))
	{
		tex->tex.y = (int)texpos & (mlx->ddat->tex[tex->or].siz.y - 1);
		texpos += tex->step;
		get_pixel(&(mlx->ddat->tex[tex->or]), tex->tex, &color);
		if (tex->screen.x > 0 && tex->screen.x < mlx->screensize.x &&
			tex->screen.y > 0 && tex->screen.y < mlx->screensize.y)
		{
			put_pixel(mlx->curr_img, (t_ivec){tex->screen.x, tex->screen.y},
							color_div(color, 1 + (ray->dist) / 3));
		}
		tex->screen.y++;
	}
}
