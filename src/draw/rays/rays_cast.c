/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_cast.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/21 18:24:40 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 22:19:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		rays_cast(t_mlx *mlx)
{
	t_ddata *ddat;
	int		screenx;

	ddat = mlx->ddat;
	screenx = 0;
	while (screenx < mlx->ddat->num_rays)
	{
		ddat->camera.x = (2.0 * screenx) / ddat->num_rays - 1.0;
		rays_setup(mlx, ddat, &(mlx->ddat->rays[screenx]));
		screenx++;
	}
	rays_draw(mlx);
}
