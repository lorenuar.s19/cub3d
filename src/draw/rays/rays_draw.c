/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_draw.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 17:13:18 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/01 22:29:29 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		put_ceil_floor(t_mlx *mlx, t_tex *tex)
{
	tex->screen.y = 0;
	while (tex->screen.y < tex->y_start)
	{
		if (tex->screen.x > 0 && tex->screen.x < mlx->screensize.x &&
			tex->screen.y > 0 && tex->screen.y < mlx->screensize.y)
		{
			put_pixel(mlx->curr_img, (t_ivec){tex->screen.x, tex->screen.y},
															mlx->fdata->ceil);
		}
		tex->screen.y++;
	}
	tex->screen.y = tex->y_end;
	while (tex->screen.y < mlx->screensize.y)
	{
		if (tex->screen.x > 0 && tex->screen.x < mlx->screensize.x &&
			tex->screen.y > 0 && tex->screen.y < mlx->screensize.y)
		{
			put_pixel(mlx->curr_img, (t_ivec){tex->screen.x, tex->screen.y},
															mlx->fdata->floor);
		}
		tex->screen.y++;
	}
}

void		rays_draw(t_mlx *mlx)
{
	t_tex	tex;
	int		screenx;

	screenx = 0;
	while (screenx < mlx->ddat->num_rays)
	{
		tex.screen.x = screenx;
		rays_draw_tex(mlx, &(mlx->ddat->rays[screenx]), &tex, screenx);
		put_ceil_floor(mlx, &tex);
		screenx++;
	}
}
