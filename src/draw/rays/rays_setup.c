/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays_setup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/29 17:24:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:55:45 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	rays_get_step(t_ddata *d, t_ray *ray)
{
	if (ray->dir.x < 0)
	{
		d->step.x = -1;
		d->side_dist.x = (d->player.pos.x - d->map.x) * d->delta_dist.x;
	}
	else
	{
		d->step.x = 1;
		d->side_dist.x = (d->map.x + 1.0 - d->player.pos.x) * d->delta_dist.x;
	}
	if (ray->dir.y < 0)
	{
		d->step.y = -1;
		d->side_dist.y = (d->player.pos.y - d->map.y) * d->delta_dist.y;
	}
	else
	{
		d->step.y = 1;
		d->side_dist.y = (d->map.y + 1.0 - d->player.pos.y) * d->delta_dist.y;
	}
}

static void	rays_check_wall(t_mlx *mlx, t_ddata *d, t_ray *ray, int *wall_hit)
{
	char	chr;
	t_map	map;
	t_ivec	pos;

	map = mlx->fdata->map;
	pos = mlx->ddat->map;
	if (pos.x < 0 || pos.x > map.siz.x || pos.y < 0 || pos.y > map.siz.y)
	{
		return ;
	}
	chr = mlx->fdata->map.tab[(pos.y * map.siz.x) + pos.x];
	if (chr == M_WALL)
	{
		*wall_hit = 1;
		ray->hit = d->side_dist;
	}
}

static void	rays_set_dist(t_ddata *d, t_ray *ray)
{
	if (ray->side == 0)
	{
		d->perp_wall_dist = (d->map.x - d->player.pos.x +
											(1 - d->step.x) / 2.0) / ray->dir.x;
	}
	else
	{
		d->perp_wall_dist = (d->map.y - d->player.pos.y +
											(1 - d->step.y) / 2.0) / ray->dir.y;
	}
	ray->dist = d->perp_wall_dist;
}

static void	rays_dda(t_mlx *mlx, t_ddata *d, t_ray *ray)
{
	int		wall_hit;

	wall_hit = 0;
	while (wall_hit == 0 &&
	d->map.x >= 0 && d->map.x < mlx->fdata->map.siz.x &&
	d->map.y >= 0 && d->map.y < mlx->fdata->map.siz.y)
	{
		if (d->side_dist.x < d->side_dist.y)
		{
			d->side_dist.x += d->delta_dist.x;
			d->map.x += d->step.x;
			ray->side = 0;
		}
		else
		{
			d->side_dist.y += d->delta_dist.y;
			d->map.y += d->step.y;
			ray->side = 1;
		}
		rays_check_wall(mlx, d, ray, &wall_hit);
	}
}

void		rays_setup(t_mlx *mlx, t_ddata *d, t_ray *ray)
{
	ray->dir.x = d->player.dir.x + d->plane.x * d->camera.x;
	ray->dir.y = d->player.dir.y + d->plane.y * d->camera.x;
	d->map = (t_ivec){
		(int)d->player.pos.x,
		(int)d->player.pos.y
	};
	d->delta_dist.x = fabs(1.0 / ray->dir.x);
	d->delta_dist.y = fabs(1.0 / ray->dir.y);
	rays_get_step(d, ray);
	rays_dda(mlx, d, ray);
	rays_set_dist(d, ray);
}
