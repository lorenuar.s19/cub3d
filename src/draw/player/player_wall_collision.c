/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_wall_collision.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:41:47 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			player_wall_collision(t_mlx *mlx, t_fvec fpos)
{
	char	chr;
	t_map	map;
	t_ivec	pos;

	map = mlx->fdata->map;
	pos = (t_ivec){(int)fpos.x, (int)fpos.y};
	if (pos.x < 0 || pos.x > map.siz.x || pos.y < 0 || pos.y > map.siz.y)
	{
		return (1);
	}
	chr = mlx->fdata->map.tab[(pos.y * map.siz.x) + pos.x];
	if (chr == M_WALL)
		return (1);
	return (0);
}
