/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_move.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:24:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	player_move_walk(t_mlx *mlx, int dir, t_pdat *player)
{
	if ((dir & FORWARD))
	{
		player->pos.x += player->dir.x * mlx->ddat->walk_speed;
		player->pos.y += player->dir.y * mlx->ddat->walk_speed;
	}
	if ((dir & BACKWARD))
	{
		player->pos.x -= player->dir.x * mlx->ddat->walk_speed;
		player->pos.y -= player->dir.y * mlx->ddat->walk_speed;
	}
	if ((dir & STRAFE_RIGHT))
	{
		player->pos.x += mlx->ddat->plane.x * mlx->ddat->walk_speed;
		player->pos.y += mlx->ddat->plane.y * mlx->ddat->walk_speed;
	}
	if ((dir & STRAFE_LEFT))
	{
		player->pos.x += mlx->ddat->plane.x * -mlx->ddat->walk_speed;
		player->pos.y += mlx->ddat->plane.y * -mlx->ddat->walk_speed;
	}
}

static void	player_move_turn(t_mlx *mlx, int dir, t_pdat *player)
{
	if ((dir & TURN_RIGHT))
	{
		player->dir = vec_rot(player->dir,
							mlx->ddat->turn_speed * mlx->ddat->rev_turning);
		mlx->ddat->plane = vec_rot(mlx->ddat->plane,
							mlx->ddat->turn_speed * mlx->ddat->rev_turning);
	}
	if ((dir & TURN_LEFT))
	{
		player->dir = vec_rot(player->dir,
							-mlx->ddat->turn_speed * mlx->ddat->rev_turning);
		mlx->ddat->plane = vec_rot(mlx->ddat->plane,
							-mlx->ddat->turn_speed * mlx->ddat->rev_turning);
	}
}

int			player_move(t_mlx *mlx)
{
	t_pdat	player;

	player = mlx->ddat->player;
	player_move_walk(mlx, mlx->ddat->dir, &player);
	player_move_turn(mlx, mlx->ddat->dir, &player);
	if (!player_wall_collision(mlx, player.pos))
	{
		mlx->ddat->player.pos = player.pos;
	}
	mlx->ddat->player.dir = player.dir;
	return (0);
}

void		player_set_direction(t_mlx *mlx, int dir, int set_or_unset)
{
	if (set_or_unset)
	{
		mlx->ddat->dir |= dir;
	}
	else
	{
		mlx->ddat->dir ^= dir;
	}
}
