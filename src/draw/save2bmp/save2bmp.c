/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save2bmp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/05 11:46:41 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	save2bmp_init_header(t_mlx *mlx, t_bmp *bmp)
{
	bmp->h.file_type = 0x4d42;
	bmp->h.file_size = 0;
	bmp->h.reserved1 = 0;
	bmp->h.reserved2 = 0;
	bmp->h.pixel_data_offset = 0x36;
	bmp->h.header_size = 0x28;
	bmp->h.image_width = mlx->screensize.x;
	bmp->h.image_height = mlx->screensize.y;
	bmp->h.planes = 1;
	bmp->h.bits_per_pixel = 0x18;
	bmp->h.compression = 0;
	bmp->h.image_size = 0;
	bmp->h.x_pixels_per_meter = 0;
	bmp->h.y_pixels_per_meter = 0;
	bmp->h.total_colors = 0;
	bmp->h.important_colors = 0;
}

static int	save2bmp_wrfileheader(t_bmp *bmp)
{
	if (write(bmp->fd, &(bmp->h.file_type), 2) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.file_size), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.reserved1), 2) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.reserved2), 2) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.pixel_data_offset), 4) == -1)
		return (error_sys_put(errno));
	return (0);
}

static int	save2bmp_wrinfoheader(t_bmp *bmp)
{
	if (write(bmp->fd, &(bmp->h.header_size), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.image_width), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.image_height), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.planes), 2) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.bits_per_pixel), 2) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.compression), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.image_size), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.x_pixels_per_meter), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.y_pixels_per_meter), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.total_colors), 4) == -1)
		return (error_sys_put(errno));
	if (write(bmp->fd, &(bmp->h.important_colors), 4) == -1)
		return (error_sys_put(errno));
	return (0);
}

static int	save2bmp_wrpixdat(t_img *img, t_bmp *bmp)
{
	t_color	color;
	t_ivec	pos;
	int		padding;

	pos.y = img->siz.y - 1;
	while (pos.y >= 0 && pos.y < img->siz.y)
	{
		pos.x = 0;
		while (pos.x >= 0 && pos.x < img->siz.x)
		{
			if (get_pixel(img, pos, &color))
				return (error_put(1, BMP4));
			if (write(bmp->fd, &color, 3) == -1)
				return (error_sys_put(errno));
			pos.x++;
		}
		color = 0;
		padding = ((img->siz.x) % 4);
		if (write(bmp->fd, &color, padding) == -1)
			return (error_sys_put(errno));
		pos.y--;
	}
	return (0);
}

int			save2bmp(t_mlx *mlx)
{
	t_bmp	bmp;

	bmp.fd = -1;
	if ((bmp.fd = open("frame.bmp",
		O_CREAT | O_WRONLY | O_TRUNC,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1)
	{
		return (error_sys_put(errno));
	}
	save2bmp_init_header(mlx, &bmp);
	if (save2bmp_wrfileheader(&bmp))
		return (error_put(1, BMP1));
	if (save2bmp_wrinfoheader(&bmp))
		return (error_put(1, BMP2));
	draw_frame(mlx);
	if (save2bmp_wrpixdat(mlx->curr_img, &bmp))
		return (error_put(1, BMP3));
	return (0);
}
