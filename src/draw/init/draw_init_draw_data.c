/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_init_draw_data.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:23:01 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void		init_dir_inverter(t_mlx *mlx)
{
	if (mlx->fdata->player.dir == NORTH || mlx->fdata->player.dir == SOUTH)
	{
		mlx->ddat->plane = (t_fvec){FOV, 0};
		mlx->ddat->rev_tex = 1;
	}
	else
	{
		mlx->ddat->plane = (t_fvec){0, FOV};
	}
	if (mlx->fdata->player.dir == SOUTH || mlx->fdata->player.dir == WEST)
	{
		mlx->ddat->rev_tex = 0;
	}
	if (mlx->fdata->player.dir == EAST)
	{
		mlx->ddat->rev_tex = 1;
	}
	if (mlx->fdata->player.dir == WEST || mlx->fdata->player.dir == SOUTH)
	{
		mlx->ddat->rev_turning = -1;
	}
}

static t_fvec	init_player_dir(t_mlx *mlx)
{
	mlx->ddat->player.pos = (t_fvec){
						mlx->fdata->player.pos.x + 0.5,
						mlx->fdata->player.pos.y + 0.5};
	mlx->ddat->rev_turning = 1;
	mlx->ddat->walk_speed = 0.001 * WALK_SPEED;
	mlx->ddat->turn_speed = 0.001 * TURN_SPEED;
	mlx->ddat->dir = 0;
	init_dir_inverter(mlx);
	if (mlx->fdata->player.dir == NORTH)
		return ((t_fvec){0, -1});
	else if (mlx->fdata->player.dir == SOUTH)
		return ((t_fvec){0, 1});
	else if (mlx->fdata->player.dir == EAST)
		return ((t_fvec){1, 0});
	else if (mlx->fdata->player.dir == WEST)
		return ((t_fvec){-1, 0});
	else
		return ((t_fvec){0, 0});
}

static void		init_minimap_data(t_mmdat *mmdat)
{
	mmdat->scale = 2;
	mmdat->player_rad = 2;
	mmdat->ray_col = build_argb(MMAPALPHA, 255, 100, 0);
	mmdat->dir_col = build_argb(MMAPALPHA, 255, 255, 0);
	mmdat->playcirc_col = build_argb(MMAPALPHA, 0, 0, 200);
	mmdat->wall_col = build_argb(MMAPALPHA, 240, 240, 240);
	mmdat->sprit_col = build_argb(MMAPALPHA, 255, 0, 0);
	mmdat->mapplayer_col = build_argb(MMAPALPHA, 0, 255, 0);
	mmdat->other_col = build_argb(MMAPALPHA, 22, 22, 22);
}

static int		init_sprit_data(t_mlx *mlx)
{
	if (!(mlx->ddat->sdat.sprit_dist = malloc(
				mlx->fdata->num_sprit * sizeof(mlx->ddat->sdat.sprit_dist))))
	{
		return (error_sys_put(errno));
	}
	if (!(mlx->ddat->sdat.sprit_order = malloc(
				mlx->fdata->num_sprit * sizeof(mlx->ddat->sdat.sprit_order))))
	{
		return (error_sys_put(errno));
	}
	return (0);
}

int				init_draw_data(t_mlx *mlx)
{
	t_ddata		*new_ddata;

	if (!mlx)
		return (error_put(1, INIT_DDATNULL));
	if (!(new_ddata = malloc(sizeof(t_ddata))))
		return (error_sys_put(errno));
	mlx->ddat = new_ddata;
	new_ddata->cell_siz = 4;
	new_ddata->player.dir = init_player_dir(mlx);
	if (new_ddata->player.dir.x == 0 && new_ddata->player.dir.y == 0)
		return (error_put(1, INIT_PDIR));
	init_minimap_data(&new_ddata->minimap);
	new_ddata->rays = NULL;
	if (rays_init(mlx))
		return (error_put(1, INIT_RAYS));
	if (draw_init_textures(mlx))
		return (error_put(1, INIT_TEXS));
	if (init_sprit_data(mlx))
		return (error_put(1, INIT_SPRIT));
	return (0);
}
