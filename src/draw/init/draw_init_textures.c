/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_init_textures.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/27 18:15:08 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:40:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			draw_init_textures(t_mlx *mlx)
{
	t_ddata	*d;
	t_fdata *fdat;
	int		i;

	d = mlx->ddat;
	fdat = mlx->fdata;
	i = 0;
	while (i < NUM_TEXTURES)
	{
		d->tex[i].img = mlx_xpm_file_to_image(mlx->ptr,
										fdat->tex_paths[i],
										&(d->tex[i].siz.x),
										&(d->tex[i].siz.y));
		d->tex[i].addr = mlx_get_data_addr(d->tex[i].img,
										&(d->tex[i].bits_per_pixel),
										&(d->tex[i].line_length),
										&(d->tex[i].endian));
		i++;
	}
	return (0);
}
