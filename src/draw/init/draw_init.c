/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_init.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:20:59 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static int		init_set_res(t_fdata *fdata, t_mlx *mlx)
{
	if (!fdata || !mlx || !mlx->ptr)
	{
		return (error_put(1, NULLMLX_ISR));
	}
	mlx_get_screen_size(mlx->ptr, &(mlx->screensize.x), &(mlx->screensize.y));
	if (fdata->res.x <= mlx->screensize.x && fdata->res.y <= mlx->screensize.y)
	{
		mlx->screensize = (t_ivec){(int)fdata->res.x, (int)fdata->res.y};
	}
	else
	{
		mlx->screensize = mlx->screensize;
	}
	return (0);
}

static t_img	*init_image(t_mlx *mlx)
{
	t_img	*new;

	if (!mlx)
	{
		return (ptrerror_put(INIT_IMGNULL));
	}
	if (!(new = malloc(sizeof(t_img))))
	{
		return (NULL);
	}
	new->img = mlx_new_image(mlx->ptr, mlx->screensize.x, mlx->screensize.y);
	new->siz = mlx->screensize;
	new->addr = mlx_get_data_addr(new->img, &(new->bits_per_pixel),
	&(new->line_length), &(new->endian));
	return (new);
}

int				draw_init(t_fdata *fdata, t_mlx *mlx, int win)
{
	*mlx = (t_mlx){win, (t_ivec){0, 0}, NULL, NULL,
										NULL, NULL, NULL, fdata, NULL};
	if (!(mlx->ptr = mlx_init()))
	{
		mlx_destroy_display(mlx->ptr);
		free(mlx->ptr);
		return (error_put(1, INIT_MLXNULL));
	}
	if (init_set_res(fdata, mlx))
	{
		return (error_put(1, INIT_RES));
	}
	mlx->curr_img = NULL;
	if (!(mlx->img1 = init_image(mlx)))
	{
		return (error_put(1, INIT_IMG1));
	}
	if (!(mlx->img2 = init_image(mlx)))
	{
		return (error_put(1, INIT_IMG2));
	}
	mlx->curr_img = mlx->img2;
	if (init_draw_data(mlx))
		return (error_put(1, INIT_DDAT));
	return (0);
}
