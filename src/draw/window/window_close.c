/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_close.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:52:13 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		window_close(t_mlx *mlx)
{
	if (!mlx)
	{
		return (error_put(1, NULLMLX_CL));
	}
	mlx_loop_end(mlx->ptr);
	return (0);
}
