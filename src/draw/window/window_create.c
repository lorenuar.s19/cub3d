/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_create.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/18 18:45:00 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int		window_create(t_mlx *mlx)
{
	if (!mlx)
	{
		return (error_put(1, NULLMLX_WC));
	}
	if (!(mlx->win = mlx_new_window(mlx->ptr, mlx->screensize.x,
											mlx->screensize.y, "Cub3D")))
	{
		return (error_put(1, MLX_NEW_WIN));
	}
	return (0);
}
