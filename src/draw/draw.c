/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 16:04:32 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 18:50:13 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void			draw_start(t_mlx *mlx)
{
	hook_sets(mlx);
	mlx_loop(mlx->ptr);
	draw_end_free(mlx);
}

int					draw(t_fdata *fdata, int show_window)
{
	static t_mlx	mlx;

	if (draw_init(fdata, &mlx, show_window))
	{
		draw_end_free(&mlx);
		return (error_put(1, DRAW_INIT));
	}
	if (mlx.show_window == 0)
	{
		if (save2bmp(&mlx))
		{
			draw_end_free(&mlx);
			return (error_put(1, SAVBMP));
		}
		draw_end_free(&mlx);
		return (0);
	}
	if (window_create(&mlx))
	{
		draw_end_free(&mlx);
		return (error_put(1, MLX_NEW_WIN));
	}
	draw_start(&(mlx));
	return (0);
}
