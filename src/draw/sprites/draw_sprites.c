/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_sprites.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/30 18:10:10 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/01 22:34:32 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	sprite_transform(t_mlx *mlx, t_sprit *s, t_ddata *d)
{
	s->transform = (t_fvec){
	s->inverse * (d->player.dir.y * s->pos.x - d->player.dir.x * s->pos.y),
	s->inverse * (-d->plane.y * s->pos.x + d->plane.x * s->pos.y)};
	s->sprit_x = (int)(mlx->screensize.x / 2.0) *
										(1 + s->transform.x / s->transform.y);
	s->h_sprit = abs((int)(mlx->screensize.y / s->transform.y));
	s->y_start = (-s->h_sprit / 2) + (mlx->screensize.y / 2);
	if (s->y_start <= 0)
		s->y_start = 0;
	s->y_end = (s->h_sprit / 2) + (mlx->screensize.y / 2);
	if (s->y_end >= mlx->screensize.y)
		s->y_end = mlx->screensize.y;
	s->w_sprit = abs((int)(mlx->screensize.y / s->transform.y));
	s->x_start = (-s->w_sprit / 2) + s->sprit_x;
	if (s->x_start <= 0)
		s->x_start = 0;
	s->x_end = (s->w_sprit / 2) + s->sprit_x;
	if (s->x_end >= mlx->screensize.x)
		s->x_end = mlx->screensize.x;
}

static void	draw_sprite_tex(t_mlx *mlx, t_sprit *s, t_ddata *d)
{
	t_color	color;
	int		div;

	sprite_transform(mlx, s, d);
	s->tex.x = (int)(1024 * (s->screen.x - (-s->w_sprit / 2 + s->sprit_x)) *
						d->tex[NUM_TEXTURES - 1].siz.x / s->w_sprit) / 1024;
	color = 0;
	if (s->transform.y > 0 &&
		s->transform.y < d->rays[s->screen.x].dist)
	{
		s->screen.y = s->y_start;
		while (s->screen.y < s->y_end &&
			s->screen.y >= 0 && s->screen.y <= mlx->screensize.y)
		{
			div = (int)((s->screen.y) * 1024 - mlx->screensize.y * 512 +
														s->h_sprit * 512);
			s->tex.y = (int)(((div * d->tex[NUM_TEXTURES - 1].siz.y) /
														s->h_sprit)) / 1024;
			get_pixel(&(d->tex[NUM_TEXTURES - 1]), s->tex, &color);
			if ((color & 0x00FFFFFF) != 0)
				put_pixel(mlx->curr_img, s->screen, color);
			s->screen.y++;
		}
	}
}

void		draw_sprites(t_mlx *mlx)
{
	t_sprit *s;
	t_ddata *d;
	int		i;

	s = &(mlx->ddat->sdat);
	d = mlx->ddat;
	get_sorted_sprites(mlx);
	s->inverse = 1.0 / (d->plane.x * d->player.dir.y -
											d->player.dir.x * d->plane.y);
	i = 0;
	while (i < mlx->fdata->num_sprit)
	{
		s->pos = (t_fvec){
			mlx->fdata->sprites[s->sprit_order[i]].x - d->player.pos.x,
			mlx->fdata->sprites[s->sprit_order[i]].y - d->player.pos.y};
		sprite_transform(mlx, s, d);
		s->screen.x = s->x_start;
		while (s->screen.x < s->x_end &&
			s->screen.x >= 0 && s->screen.x <= mlx->screensize.x)
		{
			draw_sprite_tex(mlx, s, d);
			s->screen.x++;
		}
		i++;
	}
}
