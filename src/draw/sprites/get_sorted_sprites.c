/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_sorted_sprites.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/30 20:56:21 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 14:48:51 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	swap(int *a, int *b)
{
	int		c;

	c = *a;
	*a = *b;
	*b = c;
}

static void	sort_sprites(t_mlx *mlx, t_sprit *sdat)
{
	int		num_sort;
	int		i;

	while (mlx->fdata->num_sprit)
	{
		i = 0;
		num_sort = 0;
		while (i < mlx->fdata->num_sprit - 1)
		{
			if (sdat->sprit_dist[sdat->sprit_order[i]] <
				sdat->sprit_dist[sdat->sprit_order[i + 1]])
			{
				swap(&(sdat->sprit_order[i]), &(sdat->sprit_order[i + 1]));
				num_sort++;
			}
			i++;
		}
		if (num_sort == 0)
			break ;
	}
}

void		get_sorted_sprites(t_mlx *mlx)
{
	t_fvec	pos;
	t_fvec	sprit_pos;
	int		i;

	i = 0;
	pos = mlx->ddat->player.pos;
	while (i < mlx->fdata->num_sprit)
	{
		mlx->ddat->sdat.sprit_order[i] = i;
		sprit_pos = (t_fvec){mlx->fdata->sprites[i].x,
							mlx->fdata->sprites[i].y};
		mlx->ddat->sdat.sprit_dist[i] =
							((pos.x - sprit_pos.x) * (pos.x - sprit_pos.x) +
							(pos.y - sprit_pos.y) * (pos.y - sprit_pos.y));
		i++;
	}
	sort_sprites(mlx, &(mlx->ddat->sdat));
}
