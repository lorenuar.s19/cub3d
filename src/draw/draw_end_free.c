/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_end_free.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/30 18:16:22 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 22:18:31 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	free_images(t_mlx *mlx)
{
	if (mlx && mlx->ptr && mlx->curr_img)
	{
		mlx->curr_img = NULL;
	}
	if (mlx && mlx->ptr && mlx->img1 && mlx->img1->img)
	{
		mlx_destroy_image(mlx->ptr, mlx->img1->img);
		mlx->img1->img = NULL;
		free(mlx->img1);
		mlx->img1 = NULL;
	}
	if (mlx && mlx->ptr && mlx->img2 && mlx->img2->img)
	{
		mlx_destroy_image(mlx->ptr, mlx->img2->img);
		free(mlx->img2);
		mlx->img2 = NULL;
	}
}

static void	free_textures(t_mlx *mlx)
{
	int		i;

	i = 0;
	while (mlx && mlx->ddat && i < NUM_TEXTURES)
	{
		mlx_destroy_image(mlx->ptr, mlx->ddat->tex[i].img);
		mlx->ddat->tex[i].img = NULL;
		i++;
	}
}

static void	free_sprite_data(t_mlx *mlx)
{
	if (mlx && mlx->ddat && mlx->ddat->sdat.sprit_dist)
	{
		free(mlx->ddat->sdat.sprit_dist);
		mlx->ddat->sdat.sprit_dist = NULL;
	}
	if (mlx && mlx->ddat && mlx->ddat->sdat.sprit_order)
	{
		free(mlx->ddat->sdat.sprit_order);
		mlx->ddat->sdat.sprit_order = NULL;
	}
}

void		draw_end_free(t_mlx *mlx)
{
	rays_free(mlx);
	free_images(mlx);
	free_textures(mlx);
	free_sprite_data(mlx);
	if (mlx && mlx->ptr && mlx->win)
	{
		mlx_destroy_window(mlx->ptr, mlx->win);
		mlx->win = NULL;
	}
	if (mlx && mlx->ptr)
	{
		mlx_destroy_display(mlx->ptr);
		free(mlx->ptr);
		mlx->ptr = NULL;
	}
	if (mlx && mlx->ddat)
	{
		free(mlx->ddat);
		mlx->ddat = NULL;
	}
	mlx->screensize = (t_ivec){-1, -1};
}
