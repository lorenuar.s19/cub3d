/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_sets.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/18 18:39:41 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/28 20:56:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		hook_sets(t_mlx *mlx)
{
	mlx_hook(mlx->win, XKEY_PRESS, XKEY_PRESS_MASK, hook_key_pressed, mlx);
	mlx_hook(mlx->win, XKEY_RELEASE, XKEY_RELEASE_MASK, hook_key_released,
																		mlx);
	mlx_hook(mlx->win, XCLIENT_MESSAGE, XSTRUCTURE_NOTIFY_MASK,
														mlx_loop_end, mlx->ptr);
	mlx_loop_hook(mlx->ptr, draw_frame, mlx);
}
