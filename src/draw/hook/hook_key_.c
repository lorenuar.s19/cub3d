/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_key_.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/06 16:06:39 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:51:31 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	hook_key_pressed_player(int keycode, t_mlx *mlx)
{
	if (keycode == KEY_WALK_BACKWARD)
		player_set_direction(mlx, BACKWARD, 1);
	if (keycode == KEY_WALK_FORWARD)
		player_set_direction(mlx, FORWARD, 1);
	if (keycode == KEY_TURN_RIGHT)
		player_set_direction(mlx, TURN_RIGHT, 1);
	if (keycode == KEY_TURN_LEFT)
		player_set_direction(mlx, TURN_LEFT, 1);
	if (keycode == KEY_STRAFE_LEFT)
		player_set_direction(mlx, STRAFE_LEFT, 1);
	if (keycode == KEY_STRAFE_RIGHT)
		player_set_direction(mlx, STRAFE_RIGHT, 1);
}

static void	hook_key_pressed_settings(int keycode, t_mlx *mlx)
{
	if (mlx->ddat->minimap.scale > MMAP_SCALE_MIN &&
		keycode == KEY_MINIMAP_DOWN)
	{
		mlx->ddat->minimap.scale -= MMAP_SCALE_INC;
	}
	if (keycode == KEY_MINIMAP_UP)
	{
		mlx->ddat->minimap.scale += MMAP_SCALE_INC;
	}
	if (keycode == KEY_WALK_SPEED_UP)
	{
		mlx->ddat->walk_speed += WALK_SPEED_INCREMENT * 0.0001;
	}
	if (mlx->ddat->walk_speed > 0.001 && keycode == KEY_WALK_SPEED_DOWN)
	{
		mlx->ddat->walk_speed -= WALK_SPEED_INCREMENT * 0.0001;
	}
	if (keycode == KEY_TURN_SPEED_UP)
	{
		mlx->ddat->turn_speed += TURN_SPEED_INCREMENT * 0.0001;
	}
	if (mlx->ddat->turn_speed > 0.001 && keycode == KEY_TURN_SPEED_DOWN)
	{
		mlx->ddat->turn_speed -= TURN_SPEED_INCREMENT * 0.0001;
	}
}

int			hook_key_pressed(int keycode, t_mlx *mlx)
{
	if (!mlx || !mlx->ptr)
	{
		return (error_put(1, NULLMLX_HKP));
	}
	if (keycode == XK_Escape)
	{
		mlx_loop_end(mlx->ptr);
	}
	hook_key_pressed_settings(keycode, mlx);
	hook_key_pressed_player(keycode, mlx);
	return (0);
}

static void	hook_key_released_player(int keycode, t_mlx *mlx)
{
	if (keycode == KEY_WALK_BACKWARD)
		player_set_direction(mlx, BACKWARD, 0);
	if (keycode == KEY_WALK_FORWARD)
		player_set_direction(mlx, FORWARD, 0);
	if (keycode == KEY_TURN_RIGHT)
		player_set_direction(mlx, TURN_RIGHT, 0);
	if (keycode == KEY_TURN_LEFT)
		player_set_direction(mlx, TURN_LEFT, 0);
	if (keycode == KEY_STRAFE_LEFT)
		player_set_direction(mlx, STRAFE_LEFT, 0);
	if (keycode == KEY_STRAFE_RIGHT)
		player_set_direction(mlx, STRAFE_RIGHT, 0);
}

int			hook_key_released(int keycode, t_mlx *mlx)
{
	if (!mlx)
	{
		return (error_put(1, NULLMLX_HKR));
	}
	hook_key_released_player(keycode, mlx);
	return (0);
}
