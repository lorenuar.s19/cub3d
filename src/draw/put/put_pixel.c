/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:56:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void		put_pixel(t_img *img, t_ivec pos, t_color color)
{
	char	*dest;

	if (pos.x < 0 || pos.x > img->siz.x || pos.y < 0 || pos.y > img->siz.y)
	{
		return ;
	}
	dest = img->addr +
				(pos.y * img->line_length + pos.x * (img->bits_per_pixel / 8));
	*(unsigned int*)dest = color;
}

void		put_pixel_alpha(t_img *img, t_ivec pos, t_color color)
{
	t_color	bgcol;

	if (pos.x < 0 || pos.x > img->siz.x || pos.y < 0 || pos.y > img->siz.y)
	{
		return ;
	}
	get_pixel(img, pos, &bgcol);
	put_pixel(img, pos, alpha_mult(color, bgcol));
}
