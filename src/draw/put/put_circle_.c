/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_circle_.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/19 17:06:20 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	put_circle_pixels(t_img *img, t_ivec ofs, t_ivec orig,
																t_color color)
{
	put_pixel(img, (t_ivec){orig.x + ofs.x, orig.y + ofs.y}, color);
	put_pixel(img, (t_ivec){orig.x + ofs.x, orig.y - ofs.y}, color);
	put_pixel(img, (t_ivec){orig.x - ofs.x, orig.y + ofs.y}, color);
	put_pixel(img, (t_ivec){orig.x - ofs.x, orig.y - ofs.y}, color);
	put_pixel(img, (t_ivec){orig.x + ofs.y, orig.y + ofs.x}, color);
	put_pixel(img, (t_ivec){orig.x + ofs.y, orig.y - ofs.x}, color);
	put_pixel(img, (t_ivec){orig.x - ofs.y, orig.y + ofs.x}, color);
	put_pixel(img, (t_ivec){orig.x - ofs.y, orig.y - ofs.x}, color);
}

void		put_circle(t_img *img, t_ivec origin, int radius, t_color color)
{
	int		decis;
	t_ivec	ofs;

	ofs.x = 0;
	ofs.y = 0;
	decis = 3 - (2 * radius);
	ofs.y = radius;
	put_circle_pixels(img, ofs, origin, color);
	while (radius > 0 && ofs.y >= ofs.x)
	{
		ofs.x++;
		if (decis > 0)
		{
			ofs.y--;
			decis = decis + 4 * (ofs.x - ofs.y) + 10;
		}
		else
		{
			decis = decis + 4 * ofs.x + 6;
		}
		put_circle_pixels(img, ofs, origin, color);
	}
}

void		put_circle_filled(t_img *img, t_ivec pos, int radius, t_color color)
{
	int		r;

	r = 0;
	put_circle(img, pos, 1, color);
	while (r < radius)
	{
		put_circle(img, pos, r + 1, color);
		if (r <= radius)
		{
			put_circle(img, pos, r - 1, color);
			put_circle(img, (t_ivec){pos.x + 1, pos.y + 1}, r, color);
			put_circle(img, (t_ivec){pos.x - 1, pos.y + 1}, r, color);
			put_circle(img, (t_ivec){pos.x + 1, pos.y - 1}, r, color);
			put_circle(img, (t_ivec){pos.x - 1, pos.y - 1}, r, color);
		}
		r++;
	}
}
