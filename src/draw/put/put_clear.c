/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_clear.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/25 16:18:37 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	put_clear(t_img *img)
{
	put_rect(img, img->siz, (t_ivec){0, 0}, 0);
}

void	put_clear_alpha(t_img *img)
{
	put_rect_alpha(img, img->siz, (t_ivec){0, 0}, 0);
}
