/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_rect.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/25 20:06:25 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

void	put_rect(t_img *img, t_ivec siz, t_ivec pos, t_color color)
{
	t_ivec init;

	init = pos;
	while (pos.y < init.y + siz.y && pos.y <= img->siz.y)
	{
		pos.x = init.x;
		while (pos.x < init.x + siz.x && pos.x <= img->siz.x)
		{
			put_pixel(img, pos, color);
			pos.x++;
		}
		pos.y++;
	}
}

void	put_rect_alpha(t_img *img, t_ivec siz, t_ivec pos, t_color color)
{
	t_ivec init;

	init = pos;
	while (pos.y < init.y + siz.y && pos.y <= img->siz.y)
	{
		pos.x = init.x;
		while (pos.x < init.x + siz.x && pos.x <= img->siz.x)
		{
			put_pixel_alpha(img, pos, color);
			pos.x++;
		}
		pos.y++;
	}
}
