/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_line.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 18:51:00 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/25 16:24:24 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

static void	put_line_init(t_ivec *d, t_ivec *s, t_ivec pos1, t_ivec pos2)
{
	d->x = abs(pos2.x - pos1.x);
	d->y = -abs(pos2.y - pos1.y);
	if (pos1.x < pos2.x)
	{
		s->x = 1;
	}
	else
	{
		s->x = -1;
	}
	if (pos1.y < pos2.y)
	{
		s->y = 1;
	}
	else
	{
		s->y = -1;
	}
}

void		put_line(t_img *img, t_ivec pos1, t_ivec pos2, t_color color)
{
	t_ivec	d;
	t_ivec	s;
	int		e1;
	int		e2;

	put_line_init(&d, &s, pos1, pos2);
	e1 = d.x + d.y;
	while (1)
	{
		put_pixel(img, pos1, color);
		if (pos1.x == pos2.x && pos1.y == pos2.y)
			break ;
		e2 = 2 * e1;
		if (e2 >= d.y)
		{
			e1 += d.y;
			pos1.x += s.x;
		}
		if (e2 <= d.x)
		{
			e1 += d.x;
			pos1.y += s.y;
		}
	}
}

void		put_line_alpha(t_img *img, t_ivec pos1, t_ivec pos2, t_color color)
{
	t_ivec	d;
	t_ivec	s;
	int		e1;
	int		e2;

	put_line_init(&d, &s, pos1, pos2);
	e1 = d.x + d.y;
	while (1)
	{
		put_pixel_alpha(img, pos1, color);
		if (pos1.x == pos2.x && pos1.y == pos2.y)
			break ;
		e2 = 2 * e1;
		if (e2 >= d.y)
		{
			e1 += d.y;
			pos1.x += s.x;
		}
		if (e2 <= d.x)
		{
			e1 += d.x;
			pos1.y += s.y;
		}
	}
}
