/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 09:24:07 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/31 15:47:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3d.h"

int			launch(char *path, int show_window)
{
	t_fdata	fdata;
	t_fstat	fstat;

	fstat = (t_fstat){path, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	fdata = init_fdata();
	if (parse_file(path, &fstat, &fdata))
	{
		free_fdata(&fdata);
		return (error_put(1, PARSER_ERR));
	}
	if (draw(&fdata, show_window))
	{
		free_fdata(&fdata);
		return (error_put(1, DRAW_ERR));
	}
	free_fdata(&fdata);
	return (0);
}

int			main(int argc, char *argv[])
{
	if (argc == 3 && str_ncmp(argv[2], NO_WINDOW, str_len(NO_WINDOW)) == 0)
	{
		if (launch(argv[1], 0))
			return (error_put(1, LAUNCH_ERR));
		return (0);
	}
	else if (argc == 2)
	{
		if (launch(argv[1], 1))
			return (error_put(1, LAUNCH_ERR));
		return (0);
	}
	return (error_put(EINVAL, ARG_ERR));
}
