# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/10 13:37:24 by lorenuar          #+#    #+#              #
#    Updated: 2020/12/31 15:34:47 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ================================ VARIABLES ================================= #
SHELL = bash

ifndef $(DEBUG)
	DEBUG=0
endif

OS :=	$(shell uname -s)

NAME	= cub3D
CC 		= gcc

ifeq ($(OS), Linux)
	MLX		= ./lib/mlx_linux/libmlx.a
	LIBMLX	= -lmlx
	MLXFL	= $(dir $(MLX))

	LDFLAGS = -L $(MLXFL) $(LIBMLX)
	LDFLAGS += -lXext -lX11 -lm
else
	MLX		= ./lib/mlx_mms/libmlx.dylib
	LIBMLX	= -lmlx
	MLXFL	= $(dir $(MLX))

	LDFLAGS = -L $(MLXFL) $(LIBMLX)
	LDFLAGS += -framework OpenGL -framework AppKit -lz
endif

ifeq ($(DEBUG),1)
	CFLAGS	+= -D DEBUG=1
	CFLAGS	+= -g3 -O0
	CFLAGS	+= -fsanitize=address
	ifeq ($(OS), Linux)
		CFLAGS += -fsanitize=leak
	endif
else
	CFLAGS 	= -Wall -Wextra -Werror
	CFLAGS	+= -D DEBUG=0 -D WRAP=0 -O3
endif

SRCDIR	= src/
INCDIR	= -I./inc/ -I$(MLXFL) -I/usr/include
OBJDIR	= bin/


###▼▼▼<src-updater-do-not-edit-or-remove>▼▼▼
# **************************************************************************** #
# **   Generated with https://github.com/lorenuars19/makefile-src-updater   ** #
# **************************************************************************** #

SRCS = \
	./src/parser/lintyp/lintyp_get_category.c \
	./src/parser/lintyp/lintyp_get_opt.c \
	./src/parser/lintyp/lintyp_get_map.c \
	./src/parser/check/check_exists.c \
	./src/parser/check/check_format.c \
	./src/parser/check/check_color.c \
	./src/parser/check/check_resolution.c \
	./src/parser/check/check_empty_line.c \
	./src/parser/check/check_map.c \
	./src/parser/check/check_order.c \
	./src/parser/check/check_valid_path.c \
	./src/parser/check/check_extension.c \
	./src/parser/init/init_fdata.c \
	./src/parser/init/init_map.c \
	./src/parser/map/map_get_sprites.c \
	./src/parser/map/map_get_player.c \
	./src/parser/map/map_parse_strs.c \
	./src/parser/map/map_check_closed.c \
	./src/parser/free/free_fdata.c \
	./src/parser/free/free_map.c \
	./src/parser/get/get_fdata.c \
	./src/parser/get/get_color_.c \
	./src/parser/get/get_tex_.c \
	./src/parser/get/get_resolution.c \
	./src/parser/read/read_fstats.c \
	./src/parser/read/read_lines.c \
	./src/parser/read/read_fdata.c \
	./src/parser/parse_file.c \
	./src/utils/alloc_and_free/free_str.c \
	./src/utils/alloc_and_free/free_str_error.c \
	./src/utils/alloc_and_free/alloc_zeroed.c \
	./src/utils/error/put_error.c \
	./src/utils/colors/build_color.c \
	./src/utils/colors/color_mult.c \
	./src/utils/colors/alpha_mult.c \
	./src/utils/colors/clamp.c \
	./src/utils/colors/get_color.c \
	./src/utils/stack/vect/vect_push.c \
	./src/utils/stack/vect/vect_new.c \
	./src/utils/stack/vect/vect_pop.c \
	./src/utils/stack/stack_pop.c \
	./src/utils/stack/stack_push.c \
	./src/utils/stack/stack_new.c \
	./src/utils/vec_rotate.c \
	./src/utils/strings/is_charset.c \
	./src/utils/strings/str_has.c \
	./src/utils/strings/str_start_with.c \
	./src/utils/strings/str_times_endl.c \
	./src/utils/strings/str_ncmp.c \
	./src/utils/strings/printing/put_str_fd.c \
	./src/utils/strings/printing/put_chr_fd.c \
	./src/utils/strings/get_next_line/hasto.c \
	./src/utils/strings/get_next_line/get_next_line.c \
	./src/utils/strings/get_next_line/jointo.c \
	./src/utils/strings/is_num.c \
	./src/utils/strings/str_dupli.c \
	./src/utils/strings/str_len.c \
	./src/utils/strings/is_wsp.c \
	./src/utils/numbers/read_num.c \
	./src/cub3d.c \
	./src/draw/sprites/get_sorted_sprites.c \
	./src/draw/sprites/draw_sprites.c \
	./src/draw/draw_end_free.c \
	./src/draw/init/draw_init.c \
	./src/draw/init/draw_init_draw_data.c \
	./src/draw/init/draw_init_textures.c \
	./src/draw/window/window_close.c \
	./src/draw/window/window_create.c \
	./src/draw/draw_frame.c \
	./src/draw/draw.c \
	./src/draw/minimap/minimap_draw_cell.c \
	./src/draw/minimap/minimap_draw_player.c \
	./src/draw/minimap/minimap_draw.c \
	./src/draw/rays/rays_draw.c \
	./src/draw/rays/rays_draw_tex.c \
	./src/draw/rays/rays_free.c \
	./src/draw/rays/rays_init.c \
	./src/draw/rays/get_pixel.c \
	./src/draw/rays/rays_cast.c \
	./src/draw/rays/rays_setup.c \
	./src/draw/put/put_clear.c \
	./src/draw/put/put_circle_alpha_.c \
	./src/draw/put/put_circle_.c \
	./src/draw/put/put_pixel.c \
	./src/draw/put/put_rect.c \
	./src/draw/put/put_line.c \
	./src/draw/save2bmp/save2bmp.c \
	./src/draw/player/player_wall_collision.c \
	./src/draw/player/player_move.c \
	./src/draw/hook/hook_key_.c \
	./src/draw/hook/hook_sets.c \

HEADERS = \
	./inc/cub3d.h \
	./inc/common_types.h \
	./inc/parser/parser_map_lintyps.h \
	./inc/parser/parser.h \
	./inc/parser/parser_opt_lintyps.h \
	./inc/utils.h \
	./inc/draw/draw.h \
	./inc/draw/keymap.h \
	./inc/draw/bitmap.h \

###▲▲▲<src-updater-do-not-edit-or-remove>▲▲▲

# SRCS += test.c

SRC		:= $(notdir $(SRCS))#                               Files only
OBJ		:= $(SRC:.c=.o)#                                    Files only
OBJS	:= $(addprefix $(OBJDIR), $(OBJ))#                  Full path
CSRCS	:= $(addprefix ../, $(SRCS))#                       Compiler

GR	=\033[32;1m#                                            Green
RE	=\033[31;1m#                                            Red
YE	=\033[33;1m#                                            Yellow
CY	=\033[36;1m#                                            Cyan
RC	=\033[0m#                                               Reset Colors

ifndef ($(VERBOSE))
	VERBOSE=0
endif
ifeq ($(VERBOSE), 0)
	QUIET = > /dev/null 2>&1
else
	QUIET = 2>&1
endif
# ================================== RULES =================================== #

all : $(NAME)

ifeq ($(DEBUG), 1)
run : fclean all
else
run : all
endif
	@printf "${GR}=== === === === === === VALID === === === === === ===${RC}\n"
	-./$(NAME) cubs/map4.cub ; echo 'return '$$?

valgrind :
	valgrind ./$(NAME) cubs/map4.cub

ifeq ($(DEBUG), 1)
run_parser : re all
else
run_parser : all
endif
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) --save ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) 78923hr789bn34f ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/noext ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/doesnotexist.cub ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/double.cub ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/missing.cub ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/color_out_range.cub ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/invalid_path.cub ; echo 'return '$$?
	@printf "${RE}=== === === === === SHOULD NOT RUN === === === === ===${RC}\n"
	-./$(NAME) cubs/invalid_opt/random_chars.cub ; echo 'return '$$?
	@printf "${YE}=== === === Should return an error === === === ${RC}\n"
	-./$(NAME) cubs/maps/open_map.cub ; echo 'return '$$?
	@printf "${YE}=== === === Should return an error === === === ${RC}\n"
	-./$(NAME) cubs/invalid_opt/invalid_opt.cub ; echo 'return '$$?
	@printf "${GR}=== === === === === === VALID === === === === === ===${RC}\n"
	-./$(NAME) cubs/map1.cub ; echo 'return '$$?
	@printf "${GR}=== === === === === === VALID === === === === === ===${RC}\n"
	-./$(NAME) cubs/maps/simple_map.cub ; echo 'return '$$?

test_bmp : all
	rm -f frame.bmp
	time ./$(NAME) cubs/map3.cub --save ; echo 'return '$$?

test_dirs : all
	time ./$(NAME) cubs/dirs/east.cub ; echo 'return' $$?
	time ./$(NAME) cubs/dirs/west.cub ; echo 'return' $$?
	time ./$(NAME) cubs/dirs/south.cub ; echo 'return' $$?
	time ./$(NAME) cubs/dirs/north.cub ; echo 'return' $$?

$(MLX) :
ifeq ($(OS), Linux)
	@if [[ ! -d $(MLXFL) ]];then \
	printf "${GR}VVV Downloading minilibx-linux from https://github.com/42paris/minilibx-linux.git${RC}\n" && mkdir -p ./lib && \
	git clone https://github.com/42paris/minilibx-linux.git $(MLXFL) ${QUIET} ;fi

else
	@if [[ ! -d $(MLXFL) ]];then\
	printf "${GR}VVV Downloading minilibx-mms${RC}\n" && mkdir -p $(MLXFL) && \
	curl https://projects.intra.42.fr/uploads/document/document/2186/minilibx_mms_20200219_beta.tgz -o ./lib/mlx.tgz && \
	tar -xzf ./lib/mlx.tgz --strip-components=1 -C $(MLXFL) ;fi
	@rm -f ./lib/mlx.tgz
endif
ifeq ($(OS), Linux)
	@printf "$(GR)+++ Compiling mlx $(MLX)$(RC)\n"
	@$(MAKE) -C $(MLXFL) ${QUIET}
else
	@printf "$(GR)+++ Compiling mlx $(MLX)$(RC)\n"
	@$(MAKE) -C $(MLXFL) ${QUIET}
	@cp $(MLX) ./$(notdir $(MLX))
endif

mlx_cln :
	@if [[ -d  $(MLXFL) ]]; then \
		printf "$(RE)--- Removing $(MLXFL)$(RC)\n" \
		$(MAKE) -C $(MLXFL) clean ${QUIET} \
	else \
		printf "$(YE)$(MLXFL) Was already deleted$(RC)\n" \
	;fi

mlx_re : $(MLX)
	@printf "$(YE)--- Recreating all files in $(MLXFL)$(RC)\n"
ifeq ($(OS), Linux)
	@$(MAKE) -C $(MLXFL) re ${QUIET}
else
	@$(MAKE) -C $(MLXFL) clean ${QUIET}
endif

#	linking
$(NAME)	: $(MLX) $(OBJS)
	@printf "$(CY)&&& Linking [$(LDFLAGS)]\n$(OBJ) \n\ninto $(NAME)$(RC)\n"
	@$(CC) $(OBJS) $(CFLAGS) $(LDFLAGS) -o $(NAME)

#	compiling
$(OBJS) : $(SRCS) $(HEADERS)
	@printf "$(GR)+++ Compiling [$(CFLAGS)]\n$(SRC) \nto \n$(OBJ)$(RC)\n"
	@mkdir -p $(OBJDIR)
	@$(CC) $(INCDIR) $(CFLAGS) -c $(SRCS) || mv -f $(OBJ) $(OBJDIR) $(QUIET)
	@mv $(OBJ) $(OBJDIR) $(QUIET)

#	cleaning
clean : mlx_cln
	@printf "$(RE)--- Removing \n$(OBJ)$(RC)\n"
	@rm -rf $(OBJS) $(OBJDIR)

fclean : clean
	@printf "$(RE)--- Removing \n$(NAME)$(RC)\n"
	@rm -rf $(NAME) $(OBJDIR) $(OBJ)
ifeq ($(OS), Darwin)
	@rm $(MLX) $(notdir $(MLX))
endif

rmlib :
	rm -rf ./lib

re : fclean $(MLX) all

debug :
	@echo "SRCS $(SRCS)"
	@echo "SRC $(SRC)"
	@echo "OBJS $(OBJS)"
	@echo "OBJ $(OBJ)"
	@echo "CSRCS $(CSRCS)"
	@echo "CFLAGS $(CFLAGS)"
	@echo "MLX $(MLX)"
	@echo "MLXFL $(MLXFL)"
	@echo "LDFLAGS $(LDFLAGS)"

norm :
	zsh -c "setopt extendedglob && ~/.norminette/norminette.rb **/**.[ch]~*lib*"

.PHONY	= all run clean fclean re debug mlx_cln mlx_fcln rmlib norm
