/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 09:24:35 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:48:06 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

/*
** ANSI Colors
*/
# define RST "\033[0m"
# define RED "\033[31;1m"
# define GRN "\033[32;1m"
# define YEL "\033[33;1m"
# define BLU "\033[34;1m"
# define MAG "\033[35;1m"
# define CYA "\033[36;1m"
# define WHI "\033[37;1m"

/*
** ANSI Colors Presets
*/
# define ERR "\033[91;1m"

t_fvec			vec_rot(t_fvec in, double rot);

/*
** alloc_and_free
*/
void			free_str(char **ptr);
int				free_str_error(int ret, char *s, char **str);
void			*alloc_zeroed(size_t size);

/*
** colors
*/
t_color			build_argb(t_col a, t_col r,
										t_col g, t_col b);
t_color			build_rgb(t_col r, t_col g, t_col b);
t_color			build_scolor(t_scolor col);
t_col			get_alpha(t_color color);
t_col			get_red(t_color color);
t_col			get_green(t_color color);
t_col			get_blue(t_color color);
t_scolor		get_scolor(t_color src);
t_color			color_mult(t_color color, double mult);
t_color			color_div(t_color color, double div);
t_color			alpha_mult(t_color colsrc, t_color coldst);
t_col			clamp(t_col val);

/*
** error
*/
int				error_put(int ret, char *s);
int				error_sys_put(int err);
void			*ptrerror_put(char *s);

/*
** numbers
*/
ssize_t			read_num(char **s);

/*
** stack
*/
t_stack			*stack_new(void *data);
t_stack			*stack_push(t_stack **stack, t_stack *new);
int				stack_pop(t_stack	**stack);

/*
** stack/vect
*/
t_stack			*vect_new(t_vec vec);
t_stack			*vect_push(t_stack **stack, t_stack *new);
int				vect_pop(t_stack	**stack);

/*
** strings
*/
int				is_charset(char c, char *set);
size_t			str_len(const char *s);
int				str_ncmp(const char *s1, const char *s2, size_t size);
char			*str_has(const char *s, const char *f, size_t size);
ssize_t			str_times_endl(char *s, ssize_t n, size_t len);
int				str_start_with(const char *s, const char *f);
int				is_wsp(char c);
int				is_num(char c);
char			*str_dupli(const char *to_copy);

/*
** strings/printing
*/
int				put_chr_fd(int fd, char c);
int				put_str_fd(int fd, char *s);

/*
** strings/get_next_line
*/
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 16384
# endif
# define ERR_GNL			"Internal error from get_next_line"

int				get_next_line(int fd, char **line);
char			*jointo(char *s1, char *s2, char **tofree);
size_t			hasto(char *s, char c);

#endif
