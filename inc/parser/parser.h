/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 09:24:35 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/01 23:01:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_H
# define PARSER_H

/*
** Player : storing player position and direction
*/
typedef struct		s_player_chars
{
	int				dir;
	t_vec			pos;
}					t_ch_player;

/*
** Map : storing char map data
** - siz : Vector storing the size of the map
** - tab : char array for storing the map data
*/
typedef struct		s_map
{
	t_vec			siz;
	char			*tab;
}					t_map;

/*
** struct file_stats : storing useful information about a file
** - path : path to the file
** - fd : file descriptor
** - lin_num : current line number (ignoring empty lines)
** - lin_typ : current line type
** - opt_typ : current option type
** - curr_fil_section : current sction type OPTIONS / MAP
** - tot_lin : total number of lines read
** - max_lin_len : maximum length of all lines read
** - opt_lin_num : number of lines starting with an Identifier
** - map_lin_num : nunber of lines after opt_line_num
** - map_max_len : maximum length of all map lines
*/
typedef struct		s_file_stats
{
	char			*path;
	int				fd;
	int				lin_typ;
	int				opt_typ;
	int				curr_section;
	size_t			lin_num;
	size_t			tot_lin;
	size_t			max_lin_len;
	size_t			opt_lin_num;
	size_t			map_lin_num;
	size_t			map_max_len;
}					t_fstat;

/*
** struct file_data : storing the informations about the content in the file
** - res : storing a vector for resolution
** - tex_paths[4] : an array of five strings storing wall texture paths
**                      		with the sprite texture path at the end
** - floor : a color storing the color of the floor
** - ceil : a color storing the color of the ceiling
** - map : storing map data
** - visited : a copy of the map, used in `map_check_closed()`
** - player : sotring player position and direction
** - num_sprit : number of sprites found in the map
** - sprites : positions of sprites
*/

typedef struct		s_filedata
{
	t_vec			res;
	char			*tex_paths[NUM_TEXTURES];
	t_color			floor;
	t_color			ceil;
	t_map			map;
	t_map			visited;
	t_ch_player		player;
	int				num_sprit;
	t_fvec			*sprites;
}					t_fdata;

/*
** Options
*/
# define OPT_RES	"R "
# define OPT_NORTH	"NO "
# define OPT_SOUTH	"SO "
# define OPT_WEST	"WE "
# define OPT_EAST	"EA "
# define OPT_SPRIT	"S "
# define OPT_FLOOR	"F "
# define OPT_CEIL	"C "
# define OPT_PATH	"NSWEOAS"
# define OPT_COLOR	"FC"

/*
** line categories
*/
enum				e_line_categories
{
	EMPTY,
	OPTION_RES,
	OPTION_PATH,
	OPTION_COLOR,
	MAP,
	INVALID = -1
};

/*
** Exact line types
** - LT_RES : resolution
** - LT_NORTH : texture path
** - LT_SOUTH : texture path
** - LT_WEST : texture path
** - LT_EAST : texture path
** - LT_SPRIT : texture path
** - LT_FLOOR : floor color
** - LT_CEIL : ceiling color
*/
# define NUM_TYPS 8

enum				e_exact_line_types
{
	LT_RES,
	LT_NORTH,
	LT_SOUTH,
	LT_WEST,
	LT_EAST,
	LT_SPRIT,
	LT_FLOOR,
	LT_CEIL
};

/*
** MAP
** - M_EMPTY_OUTSIDE : outside the map, set in `map_check_closed()`
** - M_EMPTY_INSIDE : inside the map, set in `map_check_closed()`
** - M_WALL : it's a wall
** - M_SPRIT : it's a sprite
** - M_NORTH & M_SOUTH & M_WEST & M_EAST : Player with initial orientation
*/
# define MAP_TYPS 7

enum				e_map_type
{
	M_EMPTY_OUTSIDE = 1,
	M_EMPTY_INSIDE,
	M_WALL,
	M_SPRIT,
	M_NORTH,
	M_SOUTH,
	M_WEST,
	M_EAST,
};

/*
** Map charsets
*/

# define MCHR_EMPTY		" 0"
# define MCHR_WALL		"1"
# define MCHR_SPRIT		"2"
# define MCHR_NORTH		"N"
# define MCHR_SOUTH		"S"
# define MCHR_WEST		"W"
# define MCHR_EAST		"E"
# define MCHR_PLAYER	"NSWE"

/*
** Error Strings
*/
# define ERROR 1
# define OPT_KEY		"File Parser : Option key is invalid"
# define BAD_EXT 		"File Parser : File path does not contain CUB extension"
# define FILE_FORMAT	"File Parser : CUB file is not well formated"
# define INVALID_CHAR	"File Parser : A line contains an invalid character"
# define UNEXP_CHAR		"File Parser : Line contains extraneous character"
# define OPT_INVAL		"File Parser : An option is missing or duplicate"
# define INVAL_TEXPATH	"File Parser : A texture path is invalid"
# define RES_OUT_RANGE	"File Parser : Resolution is negative or zero"
# define COLOR_RANGE	"File Parser : A color value is out of range"
# define BAD_MAP		"File Parser : Map is not properly formatted"
# define INVAL_LINTYP	"File Parser : Invalid line type"
# define MAP_ERR		"File Parser : Map Parser returned non zero exit code"
# define EMPTY_MAP		"Map Parser : Map contains an empty line"
# define PLAYER_ERR		"Map Parser : Map contains extraneous player"
# define PLAYER_NTFND	"Map Parser : No Player found"
# define PLAYER_OUTMAP	"Map Parser : Player position is out of the map"
# define VECT_PUSH		"Map Parser : \n\tStack : vect_push() returned NULL"
# define MCHK_BOUND		"Map Parser : Reached Map Boundary, and not a wall"
# define MCHK_OPEN		"Map Parser : Your map is not closed in 4 directions"
# define MAP_INIT_SPRIT	"Map Parser : Allocation problem in map_init_sprites()"

/*
** ************************************************************************** **
** ********************************* PARSER ********************************* **
** ************************************************************************** **
*/
int					parse_file(char *path, t_fstat *fstat, t_fdata *fdata);

/*
** parser/check
*/
int					check_format(char *line, int line_type);
int					check_color(char *line);
int					check_resolution(char *line);
int					check_valid_path(char *line);
int					check_extension(char *line);
int					check_map(char *line);
int					check_order(t_fstat *fstat, char *line);
int					check_exists(t_fstat *fstat);
int					check_empty_line(char *line);

/*
** parser/free
*/
void				free_fdata(t_fdata *fdata);
void				free_map(t_map *map);

/*
** parser/get
*/
int					get_fdata(char *line, t_fstat *fstat, t_fdata *fdata);
int					get_resolution(char *line, t_fdata *fdata);
int					get_tex_north(char *line, t_fdata *fdata);
int					get_tex_south(char *line, t_fdata *fdata);
int					get_tex_west(char *line, t_fdata *fdata);
int					get_tex_east(char *line, t_fdata *fdata);
int					get_tex_sprite(char *line, t_fdata *fdata);
int					get_col_floor(char *line, t_fdata *fdata);
int					get_col_ceil(char *line, t_fdata *fdata);

/*
** parser/init
*/
t_fdata				init_fdata(void);
char				*init_map(t_fstat *fstat, t_fdata *fdata);

/*
** parser/lintyp
*/
int					lintyp_get_category(char *line);
int					lintyp_get_opt(char *line, size_t lin_num);
int					lintyp_get_map(char map);

/*
** parser/map
*/
int					map_parse_strs(char *line, t_fdata *fdata);
int					map_get_player(t_fdata *fdata);
int					map_get_sprites(t_fdata *fdata);
int					map_check_closed(t_vec size, t_fdata *fdata, t_vec pos);

/*
** parser/read
*/
int					read_lines(char *path, t_fstat *fstat, t_fdata *fdata,
							int (sub_exec_ptr)(char *, t_fstat *, t_fdata *));
int					read_fstats(char *line, t_fstat *fstat, t_fdata *fdata);
int					read_fdata(char *line, t_fstat *fstat, t_fdata *fdata);

#endif
