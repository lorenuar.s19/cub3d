/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_opt_lintyps.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/03 21:44:54 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/10 21:40:14 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_OPT_LINTYPS_H
# define PARSER_OPT_LINTYPS_H

/*
** Global arrays for linking strings from the macros to the integer types
*/
static char		*g_checkord_str[NUM_TYPS] = {
	OPT_RES,
	OPT_NORTH,
	OPT_SOUTH,
	OPT_WEST,
	OPT_EAST,
	OPT_SPRIT,
	OPT_FLOOR,
	OPT_CEIL
};

static int		g_lintyps[NUM_TYPS] = {
	LT_RES,
	LT_NORTH,
	LT_SOUTH,
	LT_WEST,
	LT_EAST,
	LT_SPRIT,
	LT_FLOOR,
	LT_CEIL,
};

#endif
