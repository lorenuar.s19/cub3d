/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_map_lintyps.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/05 21:07:49 by lorenuar          #+#    #+#             */
/*   Updated: 2020/11/15 17:16:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_MAP_LINTYPS_H
# define PARSER_MAP_LINTYPS_H

/*
** Global arrays for linking strings from the macros to the integer types
*/
static	char		*g_map_charsets[MAP_TYPS] = {
	MCHR_EMPTY,
	MCHR_WALL,
	MCHR_SPRIT,
	MCHR_NORTH,
	MCHR_SOUTH,
	MCHR_WEST,
	MCHR_EAST
};

static	int			g_map_typs[MAP_TYPS] = {
	M_EMPTY_OUTSIDE,
	M_WALL,
	M_SPRIT,
	M_NORTH,
	M_SOUTH,
	M_WEST,
	M_EAST
};

#endif
