/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common_types.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/25 22:07:25 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/30 12:42:35 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_TYPES_H
# define COMMON_TYPES_H

# include <stddef.h>

/*
** struct vector : a handy struct when dealing with 2d coordinates
*/

typedef struct		s_vector
{
	ssize_t			x;
	ssize_t			y;
}					t_vec;

typedef struct		s_int_vector
{
	int				x;
	int				y;
}					t_ivec;

typedef struct		s_float_vector
{
	double			x;
	double			y;
}					t_fvec;

/*
** color : storing colors
*/
typedef uint16_t		t_col;

typedef struct		s_color
{
	t_col			a;
	t_col			r;
	t_col			g;
	t_col			b;
}					t_scolor;

typedef uint32_t	t_color;

# define NUM_OR 5

enum	e_directions
{
	NORTH,
	SOUTH,
	EAST,
	WEST,
	INVALID_OR
};

/*
** Stack : implementation of LIFO stack
*/
typedef struct		s_stack
{
	void			*data;
	struct s_stack	*next;
}					t_stack;

# define NUM_TEXTURES 5

/*
** textures path types
*/
enum				e_textures_path_types
{
	T_NORTH,
	T_SOUTH,
	T_WEST,
	T_EAST,
	T_SPRIT
};

#endif
