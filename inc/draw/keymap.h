/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keymap.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 22:05:22 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:34:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef KEYMAP_H
# define KEYMAP_H

/*
** X11 keycodes
*/
# define XK_LATIN1
# define XK_MISCELLANY
# include <X11/keysymdef.h>
# include <X11/X.h>

/*
** Providing an easy way to configure keys
*/

# define KEY_WALK_FORWARD		XK_Up
# define KEY_WALK_BACKWARD		XK_Down
# define KEY_TURN_LEFT			XK_Left
# define KEY_TURN_RIGHT			XK_Right
# define KEY_STRAFE_LEFT		XK_a
# define KEY_STRAFE_RIGHT		XK_d

# define KEY_MINIMAP_UP			XK_equal
# define KEY_MINIMAP_DOWN		XK_minus

# define KEY_WALK_SPEED_UP		XK_g
# define KEY_WALK_SPEED_DOWN	XK_f

# define KEY_TURN_SPEED_UP		XK_t
# define KEY_TURN_SPEED_DOWN	XK_r

#endif
