/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bitmap.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/20 14:13:07 by lorenuar          #+#    #+#             */
/*   Updated: 2020/12/29 17:35:27 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BITMAP_H
# define BITMAP_H

# include "draw/draw.h"

/*
** based on [https://miro.medium.com/max/2400/1*2ohsW8Chn2QsTcSyVcTZcw.png]
** and
** [https://medium.com/sysf/bits-to-bitmaps-a-simple-walkthrough-of-bmp-image-
** format-765dc6857393]
** had to separate the url because of the 80 chars limit
**
** 8 bits = 1 byte
** uint16_t = 2 * 8 bits = 2 bytes
** uint32_t = 4 * 8 bits = 4 bytes
**
** also, we only need to define a color palette if the Bits Per Pixel
** is less or equal to 8
*/

typedef struct	s_bitmap_file_header
{
	uint16_t	file_type;
	uint32_t	file_size;
	uint16_t	reserved1;
	uint16_t	reserved2;
	uint32_t	pixel_data_offset;
	uint32_t	header_size;
	uint32_t	image_width;
	uint32_t	image_height;
	uint16_t	planes;
	uint16_t	bits_per_pixel;
	uint32_t	compression;
	uint32_t	image_size;
	uint32_t	x_pixels_per_meter;
	uint32_t	y_pixels_per_meter;
	uint32_t	total_colors;
	uint32_t	important_colors;
}				t_bmphead;

typedef struct	s_bitmap_file
{
	int			fd;
	t_bmphead	h;
}				t_bmp;

/*
** draw/save2bmp
*/
int				save2bmp(t_mlx *mlx);
int				get_pixel(t_img *img, t_ivec pos, t_color *color);

/*
** error strings
*/
# define BMP1 "save2bmp : non zero exit code from save2bmp_wrfileheader()"
# define BMP2 "save2bmp : non zero exit code from save2bmp_wrinfoheader()"
# define BMP3 "save2bmp : non zero exit code from save2bmp_wrpixdat()"
# define BMP4 "save2bmp : non zero exit code from save2bmp_getpix()"

#endif
