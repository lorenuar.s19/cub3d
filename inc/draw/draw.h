/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 16:06:30 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:37:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DRAW_H
# define DRAW_H

/*
** enum from Xlib
** for Xevent types
*/

enum			e_x_events
{
	XKEY_PRESS = 2,
	XKEY_RELEASE = 3,
	XBUTTON_PRESS = 4,
	XBUTTON_RELEASE = 5,
	XMOTION_NOTIFY = 6,
	XENTER_NOTIFY = 7,
	XLEAVE_NOTIFY = 8,
	XFOCUS_IN = 9,
	XFOCUS_OUT = 10,
	XKEYMAP_NOTIFY = 11,
	XEXPOSE = 12,
	XGRAPHICS_EXPOSE = 13,
	XNO_EXPOSE = 14,
	XVISIBILITY_NOTIFY = 15,
	XCREATE_NOTIFY = 16,
	XDESTROY_NOTIFY = 17,
	XUNMAP_NOTIFY = 18,
	XMAP_NOTIFY = 19,
	XMAP_REQUEST = 20,
	XREPARENT_NOTIFY = 21,
	XCONFIGURE_NOTIFY = 22,
	XCONFIGURE_REQUEST = 23,
	XGRAVITY_NOTIFY = 24,
	XRESIZE_REQUEST = 25,
	XCIRCULATE_NOTIFY = 26,
	XCIRCULATE_REQUEST = 27,
	XPROPERTY_NOTIFY = 28,
	XSELECTION_CLEAR = 29,
	XSELECTION_REQUEST = 30,
	XSELECTION_NOTIFY = 31,
	XCOLORMAP_NOTIFY = 32,
	XCLIENT_MESSAGE = 33,
	XMAPPING_NOTIFY = 34,
	XGENERIC_EVENT = 35,
	XLAST_EVENT = 36,
};

enum			e_x_event_masks
{
	XNO_EVENT_MASK = (0L),
	XKEY_PRESS_MASK = (1L << 0),
	XKEY_RELEASE_MASK = (1L << 1),
	XBUTTON_PRESS_MASK = (1L << 2),
	XBUTTON_RELEASE_MASK = (1L << 3),
	XENTER_WINDOW_MASK = (1L << 4),
	XLEAVE_WINDOW_MASK = (1L << 5),
	XPOINTER_MOTION_MASK = (1L << 6),
	XPOINTER_MOTION_HINT_MASK = (1L << 7),
	XBUTTON1_MOTION_MASK = (1L << 8),
	XBUTTON2_MOTION_MASK = (1L << 9),
	XBUTTON3_MOTION_MASK = (1L << 10),
	XBUTTON4_MOTION_MASK = (1L << 11),
	XBUTTON5_MOTION_MASK = (1L << 12),
	XBUTTON_MOTION_MASK = (1L << 13),
	XKEYMAP_STATE_MASK = (1L << 14),
	XEXPOSURE_MASK = (1L << 15),
	XVISIBILITY_CHANGE_MASK = (1L << 16),
	XSTRUCTURE_NOTIFY_MASK = (1L << 17),
	XRESIZE_REDIRECT_MASK = (1L << 18),
	XSUBSTRUCTURE_NOTIFY_MASK = (1L << 19),
	XSUBSTRUCTURE_REDIRECT_MASK = (1L << 20),
	XFOCUS_CHANGE_MASK = (1L << 21),
	XPROPERTY_CHANGE_MASK = (1L << 22),
	XCOLORMAP_CHANGE_MASK = (1L << 23),
	XOWNER_GRAB_BUTTON_MASK = (1L << 24),
};

/*
** MLX image struct
** - siz : Vector storing the size
** - img : void pointer to the mlx image
** - addr : char pointer to the address of the byte of image data
** - bits_per_pixel : needed to calculate where to write pixel data
** - line_length : needed to calculate where to write pixel data
** - endian : needed to calculate where to write pixel data
*/

typedef struct	s_mlx_image
{
	t_ivec		siz;
	void		*img;
	char		*addr;
	int			bits_per_pixel;
	int			line_length;
	int			endian;
}				t_img;

typedef struct	s_draw_player_data
{
	t_fvec		pos;
	t_fvec		dir;
}				t_pdat;

# define MMAP_SCALE_MIN 0.5
# define MMAP_SCALE_INC 0.25
# define MMAPALPHA 150

typedef struct	s_minimap_data
{
	double		scale;
	int			player_rad;
	t_color		ray_col;
	t_color		dir_col;
	t_color		playcirc_col;
	t_color		wall_col;
	t_color		sprit_col;
	t_color		mapplayer_col;
	t_color		other_col;
}				t_mmdat;

typedef struct	s_ray
{
	t_fvec		hit;
	t_fvec		dir;
	double		dist;
	int			side;
}				t_ray;

typedef struct	s_texture_mapping_data
{
	t_ivec		screen;
	t_ivec		tex;
	int			or;
	int			h_wall;
	int			y_start;
	int			y_end;
	double		x_wall;
	double		step;
}				t_tex;

typedef struct	s_sprite_data
{
	double		*sprit_dist;
	int			*sprit_order;
	t_fvec		pos;
	t_fvec		transform;
	double		inverse;
	int			h_sprit;
	int			y_start;
	int			y_end;
	int			w_sprit;
	int			x_start;
	int			x_end;
	int			sprit_x;
	t_ivec		tex;
	t_ivec		screen;
}				t_sprit;

# define FOV 0.66

/*
** Struct for storing variable for drawing
*/
typedef	struct	s_drawing_data
{
	int			cell_siz;
	t_mmdat		minimap;
	t_pdat		player;
	int			rev_turning;
	double		walk_speed;
	double		turn_speed;
	int			dir;
	int			num_rays;
	t_ray		*rays;
	t_fvec		plane;
	t_fvec		camera;
	t_fvec		delta_dist;
	t_fvec		side_dist;
	double		perp_wall_dist;
	t_ivec		step;
	t_ivec		map;
	int			rev_tex;
	t_img		tex[NUM_TEXTURES];
	t_sprit		sdat;
}				t_ddata;

/*
** MLX stuct
*/
typedef struct	s_mlx
{
	int			show_window;
	t_ivec		screensize;
	void		*ptr;
	void		*win;
	t_img		*curr_img;
	t_img		*img1;
	t_img		*img2;
	t_fdata		*fdata;
	t_ddata		*ddat;
}				t_mlx;

enum			e_player_directions_keys
{
	FORWARD = 1,
	BACKWARD = (1 << 1),
	TURN_RIGHT = (1 << 2),
	TURN_LEFT = (1 << 3),
	STRAFE_LEFT = (1 << 4),
	STRAFE_RIGHT = (1 << 5),
};

/*
** Go easy on CPU, set to 0 for full framerate
*/
# define SKIPPED_FRAMES 10

/*
** draw
*/
int				draw(t_fdata *fdata, int show_window);
int				draw_frame(t_mlx *mlx);
void			draw_sprites(t_mlx *mlx);
void			draw_end_free(t_mlx *mlx);

/*
** draw/init
*/
int				draw_init(t_fdata *fdata, t_mlx *mlx, int win);
int				draw_init_textures(t_mlx *mlx);
int				init_draw_data(t_mlx *mlx);

/*
** draw/hook
*/
void			hook_sets(t_mlx *mlx);
int				hook_key_pressed(int keycode, t_mlx *mlx);
int				hook_key_released(int keycode, t_mlx *mlx);

/*
** draw/minimap
*/
void			minimap_draw(t_mlx *mlx);
void			minimap_draw_player(t_mlx *mlx);
void			draw_minimap_cell(t_mlx *mlx, t_ivec po,
												int cell_siz, t_color color);
void			minimap_draw_rays(t_mlx *mlx);

/*
** draw/player
*/
int				player_wall_collision(t_mlx *mlx, t_fvec fpos);
int				player_move(t_mlx *mlx);
void			player_set_direction(t_mlx *mlx, int dir, int set_unset);

/*
** draw/put
*/
void			put_pixel(t_img *img, t_ivec pos, t_color color);
void			put_pixel_alpha(t_img *img, t_ivec pos, t_color color);
void			put_rect(t_img *img, t_ivec siz, t_ivec pos, t_color color);
void			put_rect_alpha(t_img *img, t_ivec siz, t_ivec pos,
																t_color color);
void			put_circle(t_img *img, t_ivec origin,
												int radius, t_color color);
void			put_circle_filled(t_img *img, t_ivec pos,
												int radius, t_color color);
void			put_circle_alpha(t_img *img, t_ivec origin, int radius,
																t_color color);
void			put_circle_filled_alpha(t_img *img, t_ivec pos, int radius,
																t_color color);

void			put_line(t_img *img, t_ivec pos1, t_ivec pos2, t_color color);
void			put_line_alpha(t_img *img, t_ivec pos1, t_ivec pos2,
																t_color color);

void			put_clear(t_img *img);

/*
** draw/rays
*/
int				rays_init(t_mlx *mlx);
void			rays_free(t_mlx *mlx);
void			rays_cast(t_mlx *mlx);
void			rays_draw(t_mlx *mlx);
void			rays_setup(t_mlx *mlx, t_ddata *d, t_ray *ray);
void			rays_draw_tex(t_mlx *mlx, t_ray *ray, t_tex *tex, int screenx);

/*
** draw/sprites
*/
void			draw_sprites(t_mlx *mlx);
void			get_sorted_sprites(t_mlx *mlx);

/*
** draw/window
*/
int				window_create(t_mlx *mlx);
int				window_close(t_mlx	*mlx);

/*
** error strings
*/
# define MLX_INIT		"draw : NULL from init()"
# define MLX_NEW_WIN	"draw : NULL from mlx_new_window()"
# define INIT_MLXNULL	"draw : NULL from mlx_init()"
# define NULLMLX_CL		"draw : NULL input in winclose()"
# define NULLMLX_HKP	"draw : NULL input in hook_key_pressed()"
# define NULLMLX_HKR	"draw : NULL input in hook_key_released()"
# define NULLMLX_WC		"draw : NULL input in window_create()"
# define NULLMLX_ISR	"draw : NULL input in init_set_res()"
# define NULLMLX_DRAW	"draw ; NULL input in draw_frame()"
# define INIT_IMGNULL	"draw : NULL input in init_image()"
# define INIT_DDATNULL	"draw : NULL input in init_draw_data()"
# define RES_BIG		"draw : RES greater than screen size"
# define DRAW_WIN		"draw : non zero exit code from draw_window()"
# define DRAW_INIT		"draw : non zero exit code from draw_init()"
# define INIT_RES		"draw : non zero exit code from init_set_res()"
# define INIT_IMG1		"draw : non zero exit code from init_image() 1"
# define INIT_IMG2		"draw : non zero exit code from init_image() 2"
# define INIT_DDAT		"draw : NULL from init_draw_data()"
# define INIT_PDIR		"draw : non zero exit code from init_player_dir()"
# define INIT_RAYS		"draw : non zero exit code from rays_init()"
# define INIT_TEXS		"draw : non zero exit code from draw_init_textures()"
# define INIT_SPRIT		"draw : non zero exit code from init_sprites()"
# define SAVBMP			"draw : non zero exit code from save2bmp()"
# define XPM_SIZ		"draw : non zero exit code from read_xpm_size()"

#endif
