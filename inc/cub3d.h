/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/20 09:24:35 by lorenuar          #+#    #+#             */
/*   Updated: 2021/01/06 20:25:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H

/*
** User Settings
*/
# define WALK_SPEED 50
# define WALK_SPEED_INCREMENT 10

# define TURN_SPEED 25
# define TURN_SPEED_INCREMENT 5

/*
** Libc includes
*/
# include <errno.h>
# include <unistd.h>
# include <stdio.h>
# include <stddef.h>
# include <stdlib.h>
# include <fcntl.h>
# include <string.h>
# include <math.h>
# include <limits.h>
# include <stdint.h>
# include <sys/stat.h>

/*
** Third-party library 'minilibx'
*/
# include "mlx.h"

/*
** Project includes
*/
# include "common_types.h"
# include "utils.h"
# include "parser/parser.h"
# include "draw/draw.h"
# include "draw/keymap.h"
# include "draw/bitmap.h"

/*
** Option for saving the view as an image
*/
# define NO_WINDOW "--save"

/*
** Valid map characters
*/
# define VALID_MAP_CHARS " 012NSWE"

/*
** Error strings
*/
# define ARG_ERR		"cub3D : Args : Please provide one argument"
# define PARSER_ERR		"cub3D : non zero exit code from parse_file()"
# define DRAW_ERR		"cub3D : non zero exit code from draw()"
# define LAUNCH_ERR		"cub3D : non zero exit code from launch()"

#endif
